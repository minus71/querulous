querulous
=========


# Not an ORM
This project was made to collect the same few hundred line of code I write
every time I need to access DB data without having to resort to ORM.

Database are not made to be nice with object oriented programming, ORM is 
a good approach if you are building an application from scratch.

When you have to deal with real life production DB you will find all sort of
things inside, some which can be mapped by an ORM and some not.

Sometimes you just need to execute a bunch of sql statements and map the results,
so I came up with a class to do this mappings, then the classes became 2 , then 3,
so a GIST or a code snippet have become less attracting and I decided to put them inside
a little maven project ...

# Usage

Define an interface with all the function you want to map. The `@Adapter` annotation can be used to pass target entity class information (since JVM type erasure does not keep the List type at runtime), or, if you prefer, you can use resultsetMapper to hint the builder about wich class to use to map the resultset to the result.  

```java
@Schema(name = "dbo")
public interface Products {
	@Adapter(entityClass = Product.class)
	List<Product> getProductsList(String machines, Date from, Date to,
			Integer kind);
}
```

Once defined the interface, you call the buildAdapter to create an instance of your interface.
```java
/**
* build the api with the function builder 
*/
Products api = DBFunctionBuilder.buildAdapter(Products.class, connection);
```

The you just use the functions of you interface.

```java
List<Product> productsList = api.getProductsList("SONY3", dt0, dt1, 1);
```
## Connection
Now querulous defines an oversiplified way of managing connection, namely, if you create a query or a DBFuncion builder with a `Connection` you have to manage the connection yourself (aka _finally close it_), otherwise , if you create them with a `DataSource` connection are closed before you get the results.

The only way to support transactions now is to manage them yourself using only `Connection`.

# Test
This project was born in a Sql Server Environment, so most test are 
made using a specific DB on Sql Server.

To test this you must provide a configuration on `USER_HOME\local.conf` that is a Typesafe Config HOOCON file.

# TO DO
* Refining the scope and lifecycle of DB connection.
* Defining a way to implement transactions
* Dry up the DBFunctionBuilder code
* Wrap exceptions
