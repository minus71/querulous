package com.sorma.querulous;

import java.sql.SQLException;

public class RuntimeSQLException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7494866744622789514L;

	public RuntimeSQLException(String message, SQLException cause) {
		super(message, cause);
	}

	public RuntimeSQLException(SQLException cause) {
		super(cause);
	}
	
	

}
