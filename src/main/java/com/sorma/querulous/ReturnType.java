package com.sorma.querulous;

public enum ReturnType {
	LIST,
	VOID,
	INT,
	MULTIRESULT,
	SCALAR
}
