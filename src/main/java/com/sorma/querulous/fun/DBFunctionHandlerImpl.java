package com.sorma.querulous.fun;

import static com.sorma.querulous.helpers.DBHelpers.safeClose;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.sorma.querulous.MultiResult;
import com.sorma.querulous.ResultSetToMap;
import com.sorma.querulous.ReturnType;
import com.sorma.querulous.fun.FunctionParam.ParamType;
import com.sorma.querulous.perf.PerformanceEvent;
import com.sorma.querulous.perf.PerformanceLogger;

public class DBFunctionHandlerImpl<T> extends DBFunctionHandler<T> implements InvocationHandler {

	private static Logger logger = Logger.getLogger(DBFunctionHandlerImpl.class);
	private final Class<T> api;
	private final Map<String, DBFunctionMeta> mapping;

	public DBFunctionHandlerImpl(Class<T> api, Map<String, DBFunctionMeta> mapping) {
		this.api = api;
		this.mapping = mapping;
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		String name = method.getName();
		DBFunctionMeta dbFunctionMeta = mapping.get(name);
		if (dbFunctionMeta == null) {
			/*
			 * The session manages it's own connection so the handler is not
			 * responsible for managing the connection it just uses what he gets
			 */
			// autoClose();
			throw new UnsupportedOperationException("Method " + method + " not mapped on DB");
		} else {
			// try {
			logger.debug("Prepare call " + dbFunctionMeta);
			List<FunctionParam> parameters = dbFunctionMeta.getParameters();
			Map<String, Object> argMap = new LinkedHashMap<String, Object>();
			int i = 0;

			for (FunctionParam functionParam : parameters) {
				ParamType pType = functionParam.getParamType();
				if (pType == ParamType.IN || pType == ParamType.INOUT) {
					String name2 = functionParam.getName();
					argMap.put(name2, i < args.length ? args[i] : null);
					i++;
				}
			}
			Object result = executeQuery(argMap, dbFunctionMeta, getConnection());
			return result;
			// } finally {
			// autoClose();
			// }
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private Object executeQuery(Map<String, Object> arguments, DBFunctionMeta fn, Connection conn) throws SQLException {

		Object results = null;
		Map<String, FunctionParam> parameters = buildParametersMap(fn);

		String paramsString = buildParamString(parameters.size());

		ReturnType retType = fn.getReturnType();
		String statement = fn.isFunction()
				? fn.isScalar() ? String.format("select %s(%s)", fn.getQualifiedName(), paramsString)
						: String.format("select * from %s(%s)", fn.getQualifiedName(), paramsString)
				: retType == ReturnType.INT || retType == ReturnType.MULTIRESULT || retType == ReturnType.LIST
						? String.format("{? = call %s(%s)}", fn.getQualifiedName(), paramsString)
						: String.format("{call %s(%s)}", fn.getQualifiedName(), paramsString);

		logger.debug("Preparing statement: " + statement);

		// try {

		PreparedStatement stmt = null;
		PerformanceEvent evt = null;
		try {
			evt = onBeginCallable(statement, arguments);

			if (fn.getReturnType() == ReturnType.LIST || fn.getReturnType() == ReturnType.SCALAR) {
				if (fn.isFunction()) {
					stmt = conn.prepareStatement(statement);
				} else {
					stmt = conn.prepareCall(statement);
				}
				bindParameters(arguments, parameters, stmt, !fn.isFunction());
				List resultList = new ArrayList();
				results = resultList;
				// ResultSet rsCS = stmt.executeQuery();

				ResultSetToMap rsHandler = new ResultSetToMap();
				Function<ResultSet, ? extends Object> mapper = fn.getMapper() == null ? rsHandler : fn.getMapper();

				ResultSet rsCS;
				boolean isRS = stmt.execute();
				if (isRS) {
					rsCS = stmt.getResultSet();
				} else {
					((CallableStatement) stmt).getInt(1);
					stmt.getMoreResults();
					rsCS = stmt.getResultSet();
				}

				while (rsCS != null && rsCS.next()) {
					if(retType==ReturnType.SCALAR){
						results = mapper.apply(rsCS);
					}else{
						resultList.add(mapper.apply(rsCS));
					}
				}
			} else if (fn.getReturnType() == ReturnType.VOID) {
				stmt = conn.prepareStatement(statement);
				bindParameters(arguments, parameters, stmt, false);
				stmt.execute();
			} else if (fn.getReturnType() == ReturnType.INT) {
				CallableStatement cs = conn.prepareCall(statement);
				stmt = cs;
				bindParameters(arguments, parameters, cs, true);

				cs.registerOutParameter(1, Types.INTEGER);
				cs.execute();
				Integer value = cs.getInt(1);
				results = value;
			} else if (fn.getReturnType() == ReturnType.MULTIRESULT) {
				MultiResult mr = new MultiResult();
				results = mr;
				CallableStatement cs = conn.prepareCall(statement);
				stmt = cs;

				bindParameters(arguments, parameters, cs, true);
				cs.registerOutParameter(1, Types.INTEGER);

				// -------------------------BEGIN -------------------
				boolean isResultset = cs.execute();

				boolean first = true;
				while (first || cs.getMoreResults()) {
					if (isResultset) {
						ResultSet resultSet = cs.getResultSet();
						ResultSetToMap handler = new ResultSetToMap();
						if (resultSet != null) {
							List<Map<String, Object>> rowset = new ArrayList<Map<String, Object>>();
							mr.getResults().add(rowset);
							while (resultSet.next()) {
								Map<String, Object> row = handler.apply(resultSet);
								rowset.add(row);
							}
						}
					} else {
						Integer value = cs.getInt(1);
						mr.setValue(value);
					}
					first = false;
				}
				if (isResultset) {
					Integer value = cs.getInt(1);
					mr.setValue(value);
				}
			}

			// -------------------------END -------------------
		} finally {
			safeClose(stmt);
			if (evt != null) {
				evt.stop();
			}
		}

		return results;
	}

	private static PerformanceLogger perfLog = PerformanceLogger.getInstance();

	protected PerformanceEvent onBeginCallable(String stmt, Map<String, Object> args) {
		PerformanceEvent evt = perfLog.onBegin();
		evt.setName("CALLABLE");
		evt.setAttr("STMT", stmt);
		evt.setAttr("PARAMS", args);
		return evt;
	}

	private void bindParameters(Map<String, Object> arguments, Map<String, FunctionParam> parameters,
			PreparedStatement stmt, boolean bindResultValue) throws SQLException {
		int i = 0;
		if (stmt instanceof CallableStatement) {
			CallableStatement cs = (CallableStatement) stmt;
			cs.registerOutParameter(1, Types.INTEGER);
			i++;
		}

		for (String key : parameters.keySet()) {
			i++;
			Object arg = arguments.get(key);
			FunctionParam param = parameters.get(key);
			logger.debug("Binding " + param + " to " + arg);
			Class type = param.getJavaType();
			if (arg instanceof Date) {
				arg = new Timestamp(((Date) arg).getTime());
			}
			if (arg == null || type.isAssignableFrom(arg.getClass())) {
				if (type == String.class) {
					stmt.setString(i, (String) arg);
				} else if (type == Integer.class) {
					stmt.setInt(i, (Integer) arg);
				} else if (type == Timestamp.class) {
					stmt.setTimestamp(i, (Timestamp) arg);
				} else {
					try {
						stmt.setObject(i, arg);
					} catch (SQLException e) {
						String message = "Error binding " + param + " to " + arg;
						logger.error(message);
						throw new RuntimeException(message, e);
					}
				}
			}
		}
	}

	private Map<String, FunctionParam> buildParametersMap(DBFunctionMeta fn) {
		Map<String, FunctionParam> parameters = new LinkedHashMap<String, FunctionParam>();
		for (FunctionParam functionParam : fn.getParameters()) {
			ParamType pType = functionParam.getParamType();
			if (pType == ParamType.IN || pType == ParamType.INOUT) {
				parameters.put(functionParam.getName().toLowerCase(), functionParam);
			}
		}
		return parameters;
	}

	private String buildParamString(int n) {
		String[] qms = new String[n];
		Arrays.fill(qms, "?");
		String paramsString = Joiner.on(",").join(qms);
		return paramsString;
	}
}
