package com.sorma.querulous.fun;

import com.google.common.base.Function;

public class Trim implements Function<String, String> {

	@Override
	public String apply(String input) {
		return input.trim();
	}

}
