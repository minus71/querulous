package com.sorma.querulous.fun;

public class FunctionParam {
	private String name;
	private Class javaType;
	private ParamType paramType = ParamType.IN;
	
	public ParamType getParamType() {
		return paramType;
	}
	public void setParamType(ParamType paramType) {
		this.paramType = paramType;
	}

	public enum ParamType{
		IN,
		OUT,
		INOUT,
		RESCOL,
		UNKNOWN,
	}
	
	public FunctionParam(String name, Class javaType, ParamType paramType) {
		super();
		this.name = name;
		this.javaType = javaType;
		this.paramType = paramType;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Class getJavaType() {
		return javaType;
	}
	public void setJavaType(Class javaType) {
		this.javaType = javaType;
	}
	@Override
	public String toString() {
		return "FunctionParam [name=" + name + ", paramType=" + paramType
				+ ", javaType=" + javaType + "]";
	}
	

	
	
}
