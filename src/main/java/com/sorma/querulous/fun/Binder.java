package com.sorma.querulous.fun;

import static java.lang.String.format;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.sorma.querulous.ReturnCode;

public class Binder {

	private static Logger logger = Logger.getLogger(Binder.class);

	public static void bind(PreparedStatement stmt, Map<String, Object> paramsMap, Collection<String> parameterList)
			throws SQLException {
		logger.debug("Binding parameters");
		int index = 0;

		for (String paramName : parameterList) {
			index++;
			Object value = paramsMap.get(paramName);
			if(value instanceof TypedNullValue) {
				TypedNullValue wrapper = (TypedNullValue) value;
				wrapper.setNullValue(stmt, index);
			} else if (value == null) {
				logger.debug(format("ARG %-15s[%3d] is null ", paramName, index));
				// trying to pass a null param as a null string.
				stmt.setNull(index, Types.VARCHAR);
			} else {
				logger.debug(format("ARG %-15s[%3d] = %s", paramName, index,
						value));
				if (value instanceof String) {
					stmt.setString(index, (String) value);
				} else if (value instanceof BigDecimal) {
					stmt.setBigDecimal(index, (BigDecimal) value);
				} else if (value instanceof Integer) {
					stmt.setInt(index, (Integer) value);
				} else if (value instanceof Long) {
					stmt.setLong(index, (Long) value);
				} else if (value instanceof Double) {
					stmt.setDouble(index, (Double) value);
				} else if (value instanceof Float) {
					stmt.setFloat(index, (Float) value);
				} else if (value instanceof Date) {
					Date d = (Date) value;
					stmt.setTimestamp(index, new Timestamp(d.getTime()));
				} else if (value instanceof ReturnCode) {
					stmt.setNull(index, Types.INTEGER);
				} else {
					stmt.setObject(index, value);
				}
			}
		}
	}
	
	
	
	
	
	
	
	
}
