package com.sorma.querulous.fun;

import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.sorma.querulous.fun.FunctionParam.ParamType;
import com.sorma.querulous.session.Session;

public abstract class DBFunctionHandler<T> implements InvocationHandler{

	private ThreadLocal<WeakReference<Session>> _session = new ThreadLocal<WeakReference<Session>>();

	public abstract Object invoke(Object proxy, Method method, Object[] args) throws Throwable;

	public DBFunctionHandler() {
		super();
	}

	protected void use(Session session) {
		_session.set(new WeakReference<Session>(session));
	}

	protected Connection getConnection() {
		WeakReference<Session> sessionRef = _session.get();
		if(sessionRef!=null){
			Session session = sessionRef.get();
			if(session!=null){
				return session.getConnection();
			}
		} 
		
		throw new RuntimeException("NO active connection.");
	}

}