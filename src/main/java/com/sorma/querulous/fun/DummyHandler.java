package com.sorma.querulous.fun;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class DummyHandler<T> extends DBFunctionHandler<T>{

	private Class<T> clazz;
	private Exception e;
	
	
	
	public DummyHandler(Class<T> clazz) {
		super();
		this.clazz = clazz;
	}

	public DummyHandler(Class<T> clazz, Exception e) {
		super();
		this.clazz = clazz;
		this.e=e;
	}



	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		if(e!=null){
			throw new UnsupportedOperationException(clazz+" was not built.",e);
		}else{
			throw new UnsupportedOperationException(clazz+" was not built.");
		}
	}

}
