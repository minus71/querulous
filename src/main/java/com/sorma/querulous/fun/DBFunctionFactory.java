package com.sorma.querulous.fun;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Proxy;
import java.lang.reflect.Type;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.google.common.base.Function;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.sorma.querulous.MultiResult;
import com.sorma.querulous.ResultSetToEntity;
import com.sorma.querulous.ResultSetToMap;
import com.sorma.querulous.ReturnType;
import com.sorma.querulous.annotation.Adapter;
import com.sorma.querulous.annotation.DB;
import com.sorma.querulous.annotation.Schema;
import com.sorma.querulous.fun.FunctionParam.ParamType;
import com.sorma.querulous.session.Session;

public class DBFunctionFactory {


	private static Logger logger = Logger.getLogger(DBFunctionFactory.class);



	Cache<Class, DBFunctionHandler> loadingCache =
		CacheBuilder
			.newBuilder()
			.concurrencyLevel(4)
			.expireAfterWrite(5, TimeUnit.MINUTES)
			.build();







	private <T> DBFunctionHandler<T> createHandler(Class<T> api, Connection connection) throws SQLException{
		Map<String, DBFunctionMeta> mapping = buildMapping(api, connection);
		DBFunctionHandler<T> handler = new DBFunctionHandlerImpl<T>(api, mapping);
		return handler;
	}


	private <T> Map<String, DBFunctionMeta> buildMapping(Class<T> api, Connection connection) throws SQLException {
		Map<String, DBFunctionMeta> mapping = new HashMap<String, DBFunctionMeta>();

		Method[] methods = api.getMethods();

		Schema schemaAnnotation = api.getAnnotation(Schema.class);
		DB dbAnnotation = api.getAnnotation(DB.class);
		String schema = schemaAnnotation == null ? null : schemaAnnotation.value();
		String db = dbAnnotation == null ? null : dbAnnotation.value();

		DatabaseMetaData metaData = connection.getMetaData();
		for (Method method : methods) {
			String name = method.getName();
			ResultSet proc = metaData.getProcedures(db, schema, name);
			boolean found = false;
			DBFunctionMeta functionDef = null;
			while (proc.next() && !found) {
				ResultSetToMap rm = new ResultSetToMap();
				Map<String, Object> procMeta = rm.apply(proc);
				ResultSet rscol = metaData.getProcedureColumns((String)procMeta.get("PROCEDURE_CAT"), (String) procMeta.get("PROCEDURE_SCHEM"),
						(String) procMeta.get("PROCEDURE_NAME"), null);

				functionDef = procArgs(name, rscol);
				functionDef.setDb((String)procMeta.get("PROCEDURE_CAT"));
				functionDef.setSchema((String)procMeta.get("PROCEDURE_SCHEM"));

				com.sorma.querulous.annotation.TableValuedFunction funAnnot = method.getAnnotation(com.sorma.querulous.annotation.TableValuedFunction.class);
				if(funAnnot!=null){
					functionDef.setFunction(true);
					functionDef.setScalar(false);
				}

				com.sorma.querulous.annotation.ScalarValuedFunction funScalarAnnot = method.getAnnotation(com.sorma.querulous.annotation.ScalarValuedFunction.class);
				if(funScalarAnnot!=null){
					functionDef.setFunction(true);
					functionDef.setScalar(true);
				}

				mapping.put(name, functionDef);
				found = true;
			}


			if(found){
				if(functionDef.isScalar()){
					functionDef.setReturnType(ReturnType.SCALAR);
				}else{
				Class<?> returnType = method.getReturnType();
				if(returnType.equals(Void.class)){
					functionDef.setReturnType(ReturnType.VOID);
				}else if(returnType == int.class ||
						returnType == long.class ||
						Number.class.isAssignableFrom(returnType)){
					functionDef.setReturnType(ReturnType.INT);
				}else if((returnType == MultiResult.class)){
					functionDef.setReturnType(ReturnType.MULTIRESULT);
				}
				}


				Adapter adapter = method.getAnnotation(Adapter.class);
				if(functionDef.isScalar()){
					ResultSetToEntity<? extends Object> instance = new ResultSetToEntity(method.getReturnType());
					functionDef.setMapper(instance);
				}else if (adapter != null) {
					Class<? extends Function<ResultSet, ? extends Object>> rsMapper = adapter.resultsetMapper();
					if (!rsMapper.equals(Adapter.DummyMapper.class)) {
						try {
							functionDef.setMapper(rsMapper.newInstance());
						} catch (Exception e) {
							logger.error(rsMapper.getName() + " cannot be instanced as a resultset mapper.", e);
						}
					} else {
						Class<T> entityClass = (Class<T>) adapter.entityClass();
						if (!entityClass.equals(Void.class)) {
							functionDef.setMapper(new ResultSetToEntity<T>(entityClass));
						}
					}
				} else {
					Type genericReturnType = method.getGenericReturnType();
					if (genericReturnType instanceof ParameterizedType) {
						ParameterizedType pType = (ParameterizedType) genericReturnType;
						Type rawType = pType.getRawType();
						Type[] actualTypeArguments = pType.getActualTypeArguments();
						if(actualTypeArguments !=null && actualTypeArguments.length == 1){
							if(rawType instanceof Class){
								if(((Class) rawType).isAssignableFrom(List.class)){
									Type listType = actualTypeArguments[0];
									if(listType instanceof Class){
										functionDef.setMapper(new ResultSetToEntity<T>((Class)listType));
									}
								}
							}
						}

					}
				}
			}
		}
		return mapping;
	}


	private DBFunctionMeta procArgs(String name, ResultSet paramsRes) throws SQLException {
		DBFunctionMeta dbFunctionMeta = new DBFunctionMeta(name);
		while (paramsRes.next()) {
			String pName = paramsRes.getString("COLUMN_NAME");
			int pType = paramsRes.getShort("COLUMN_TYPE");
			FunctionParam.ParamType colType;
			if (DatabaseMetaData.procedureColumnIn == pType) {
				colType = ParamType.IN;
			} else if (DatabaseMetaData.procedureColumnOut == pType) {
				colType = ParamType.OUT;
			} else if (DatabaseMetaData.procedureColumnInOut == pType) {
				colType = ParamType.INOUT;
			} else if (DatabaseMetaData.procedureColumnReturn == pType) {
				colType = ParamType.RESCOL;
			} else {
				colType = ParamType.UNKNOWN;
			}
			Class colDataType = null;
			if (!"@TABLE_RETURN_VALUE".equals(pName)) {

				int pDataType = paramsRes.getInt("DATA_TYPE");
				switch (pDataType) {
				case Types.INTEGER:
					colDataType = Integer.class;
					break;
				case Types.TIMESTAMP:
					colDataType = Timestamp.class;
					break;
				case Types.VARCHAR:
				case Types.CHAR:
				case Types.NVARCHAR:
				case Types.NCHAR:
					colDataType = String.class;
					break;
				default:
					colDataType = Object.class;
					break;
				}
			} else {
				colType = ParamType.UNKNOWN;
			}

			dbFunctionMeta.addParam(new FunctionParam(pName.substring(1).toLowerCase(), colDataType, colType));

		}
		return dbFunctionMeta;
	}



	@SuppressWarnings("unchecked")
	public <T> T getAPI(final Class<T> api, final Session session){
		try {
			DBFunctionHandler<T> handler = getHandler(api, session.getConnection());
			handler.use(session);
			return (T) Proxy.newProxyInstance(api.getClassLoader(), new Class[] { api },
					handler);
		} catch (ExecutionException e) {
			logger.error("Error fetching handler for "+api,e);
		}
		return null;
	}


	@SuppressWarnings({ "unchecked", "rawtypes" })
	private <T> DBFunctionHandler<T> getHandler(final Class<T> api, final Connection connection) throws ExecutionException {
		return loadingCache.get(api, new Callable<DBFunctionHandler>() {
			@SuppressWarnings("rawtypes")
			@Override
			public DBFunctionHandler call() throws Exception {
				DBFunctionHandler<T> handler ;
				try{
					handler = createHandler(api, connection);
				} catch (Exception e) {
					logger.error("Error building handler for class "+api+" using connection "+connection);
					handler = new DummyHandler<T>(api,e);
				}
				return handler;
			}
		});
	}
}
