package com.sorma.querulous.fun;

import java.sql.Connection;
import java.sql.SQLException;

public interface AfterInsert<T> {
	
	
	/**
	 * Updates the entity after an insert, generally used to update the id 
	 * when auto generated.
	 * 
	 * Updating the current entity or creating a new one is implementation specific.
	 * 
	 * @param sourceEntity The inserted entity 
	 * @param conn the current connection
	 * @return The updated entity.
	 * @throws SQLException
	 */
	T updateEntity(T sourceEntity, Connection conn) throws SQLException;
}
