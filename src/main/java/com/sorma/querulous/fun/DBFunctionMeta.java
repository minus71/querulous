package com.sorma.querulous.fun;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Function;
import com.sorma.querulous.ReturnType;

public class DBFunctionMeta {
	private String db;
	private String schema;
	private String name;
	private boolean function=false;
	private boolean scalar=false;
	private ReturnType returnType=ReturnType.LIST;
	private Function<ResultSet, ? extends Object> mapper = null;

	public boolean isFunction() {
		return function;
	}
	public void setFunction(boolean function) {
		this.function = function;
	}

	private List<FunctionParam> parameters = new ArrayList<FunctionParam>();

	public DBFunctionMeta(String name) {
		super();
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<FunctionParam> getParameters() {
		return parameters;
	}
	public void setParameters(List<FunctionParam> parameters) {
		this.parameters = parameters;
	}

	public void addParam(FunctionParam fnParam){
		parameters.add(fnParam);

	}
	public ReturnType getReturnType() {
		return returnType;
	}
	public void setReturnType(ReturnType returnType) {
		this.returnType = returnType;
	}
	public void setMapper(Function<ResultSet, ? extends Object> newInstance) {
		this.mapper=newInstance;
	}
	public Function<ResultSet, ? extends Object> getMapper() {
		return mapper;
	}
	public String getSchema() {
		return schema;
	}
	public void setSchema(String schema) {
		this.schema = schema;
	}
	public String getDb() {
		return db;
	}
	public void setDb(String db) {
		this.db = db;
	}

	public String getQualifiedName(){
		String dbPrefix = db == null
				? ""
				: db +".";
		String schemaPrefix = schema == null
				? ""
				: schema +".";
		return dbPrefix + schemaPrefix +name;


	}
	public boolean isScalar() {
		return scalar;
	}
	public void setScalar(boolean scalar) {
		this.scalar = scalar;
	}
}
