package com.sorma.querulous.fun;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class TypedNullValue {

	public final Class<?> nullType;

	public TypedNullValue(Class<?> nullType) {
		super();
		this.nullType = nullType;
	}

	
	
	public void setNullValue(PreparedStatement stmt, int idx) throws SQLException {
		
		Class<?> type = nullType;
		int sqlType;
		if(type.isAssignableFrom(String.class)){
			sqlType = Types.VARCHAR;
		}else if (type.isAssignableFrom(Double.class)) {
			sqlType = Types.DOUBLE;
		}else if (type.isAssignableFrom(Float.class)) {
			sqlType = Types.FLOAT;
		}else if (type.isAssignableFrom(Date.class)) {
			sqlType = Types.TIMESTAMP;
		}else if (type.isAssignableFrom(Integer.class)){
			sqlType = Types.INTEGER;
		}else if (type.isAssignableFrom(Long.class)){
			sqlType = Types.INTEGER;
		}else if (type.isAssignableFrom(Boolean.class)){
			sqlType = Types.BIT;
		}else if (type.isAssignableFrom(BigDecimal.class)){
			sqlType = Types.DECIMAL;
		}else if (type.isAssignableFrom(byte[].class)){
			sqlType = Types.VARBINARY;
		}else {
			// Dubious
			sqlType = Types.OTHER;
		}
		
		stmt.setNull(idx, sqlType);
	}
	
	private static Map<Class<?>,TypedNullValue> _cache = Collections.synchronizedMap(new HashMap<Class<?>,TypedNullValue>());
	
	public static final TypedNullValue from(Class<?> clazz) {
		TypedNullValue typedNullValue = _cache.get(clazz);
		if(typedNullValue==null) {
			typedNullValue = new TypedNullValue(clazz);
			_cache.put(clazz, typedNullValue);
		}
		return typedNullValue;
	}
	
	
	
}
