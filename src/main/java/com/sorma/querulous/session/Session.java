package com.sorma.querulous.session;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;

import com.sorma.querulous.EntityValueMapper;
import com.sorma.querulous.ParsedStatement;
import com.sorma.querulous.Query;
import com.sorma.querulous.fun.AfterInsert;

public interface Session {

	void beginTransaction(Integer isolationLevel) throws SQLException;

	void beginTransaction() throws SQLException;

	void commit() throws SQLException;

	void rollback() throws SQLException;

	void close() throws SQLException;

	String toString();

	boolean isClosed();

	<T> T insert(T entity) throws SQLException;

	<T> T update(T entity) throws SQLException;

	Query query(String statement) throws SQLException;

	Query queryWithValues(String statement, String listName, Collection values) throws SQLException;

	Query queryWithValues(String statement, String listName, Collection values, String listName2, Collection values2)
			throws SQLException;

	<T> T buildFunctionsAPI(Class<T> class1) throws SQLException;

	<T> T useAPI(Class<T> clazz);
	Connection getConnection();
	ParsedStatement parse(String sqlstatement);
	
}