package com.sorma.querulous.session;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.log4j.Logger;

import com.sorma.querulous.DBFunctionBuilder;
import com.sorma.querulous.DefaultValueMapper;
import com.sorma.querulous.EntityValueMapper;
import com.sorma.querulous.ParsedStatement;
import com.sorma.querulous.Persister;
import com.sorma.querulous.Query;
import com.sorma.querulous.fun.AfterInsert;
import com.sorma.querulous.fun.DBFunctionFactory;
import com.sorma.querulous.helpers.CollectionValuedStatement;


public abstract class AbstractSession implements Session{

	public final Connection connection;
	private CloseListener closeListener;
	private boolean closed = false;
	private boolean defaultAutocommit;
	private boolean actualAutocommit;
	private Integer restorableIsolationLevel;
	private DBFunctionFactory factory;
	private StatementPreParser preParser;
	private EntityValueMapper valueMapper = new DefaultValueMapper();
	private static Logger logger = Logger.getLogger(AbstractSession.class);

	
	
	public AbstractSession(Connection connection, CloseListener closeListener, Boolean autocommit, DBFunctionFactory factory) {
		super();
		this.connection = connection;
		this.closeListener = closeListener;
		this.defaultAutocommit = autocommit;
		this.actualAutocommit = autocommit;
		this.factory=factory;
		logger.debug("Starting session "+this);
	}


	@Override
	protected void finalize() throws Throwable {
		if(!connection.isClosed()){
			logger.warn(String.format("Session %s was not closed. Releasing connection %s",this,connection));
			try{
				if(!connection.getAutoCommit()){
					connection.rollback();
				}
			} finally {
				closeConnection();
			}
		}
	}

	protected void closeConnection() throws SQLException {
		try{
			logger.debug("Closing connection "+connection);
			connection.close();
			closed = true;
		} finally {
			if(closeListener!=null){
				closeListener.onClose(this);
			}
		}
	}


	/* (non-Javadoc)
	 * @see com.sorma.querulous.session.Session#beginTransaction(java.lang.Integer)
	 */
	@Override
	public void beginTransaction(Integer isolationLevel) throws SQLException{
		logger.debug("Starting Transaction");
		if(actualAutocommit){
			actualAutocommit = false;
			connection.setAutoCommit(actualAutocommit);
			logger.debug("Autocommit unset.");
		}

		if(isolationLevel!=null){
			logger.debug("Setting isolation level to :"+isolationLevel);

			int currentIsolationLevel = connection.getTransactionIsolation();
			restorableIsolationLevel = currentIsolationLevel;
			connection.setTransactionIsolation(isolationLevel);
		}


	}
	/* (non-Javadoc)
	 * @see com.sorma.querulous.session.Session#beginTransaction()
	 */
	@Override
	public void beginTransaction() throws SQLException{
		beginTransaction(null);
	}

	/* (non-Javadoc)
	 * @see com.sorma.querulous.session.Session#commit()
	 */
	@Override
	public void commit() throws SQLException{
		if(closed){
			logger.error(this+" already closed");
		}
		if(!actualAutocommit){
			connection.commit();
		}
	}

	/* (non-Javadoc)
	 * @see com.sorma.querulous.session.Session#rollback()
	 */
	@Override
	public void rollback() throws SQLException{
		if(closed){
			logger.error(this+" already closed");
		}
		if(!actualAutocommit){
			logger.debug("Rollback transaction on "+this);
			connection.rollback();
		}else{
			logger.debug("Rollback called on "+this+" but session is in autocommit mode.");
		}
	}


	/* (non-Javadoc)
	 * @see com.sorma.querulous.session.Session#close()
	 */
	@Override
	public void close() throws SQLException{
		logger.debug("Closing session "+this);
		if(!closed){
			try{
				if(!connection.isClosed()){
					if(restorableIsolationLevel!=null){
						int actualIsolationLevel = connection.getTransactionIsolation();
						logger.debug("Restore Isolation level from "+actualIsolationLevel+" to "+restorableIsolationLevel);
						connection.setTransactionIsolation(actualIsolationLevel);
					}

					if(!actualAutocommit){
						connection.commit();
					}
					if(actualAutocommit!=defaultAutocommit){
						logger.debug("Clean up autocommit");
						connection.setAutoCommit(defaultAutocommit);
					}
				}
			}finally{

				closeConnection();
			}
		}else{
			logger.warn(this+" is already closed");
			// NOOP
		}
	}



	/* (non-Javadoc)
	 * @see com.sorma.querulous.session.Session#toString()
	 */
	@Override
	public String toString() {
		return "Session [connection=" + connection + "]";
	}


	/* (non-Javadoc)
	 * @see com.sorma.querulous.session.Session#isClosed()
	 */
	@Override
	public boolean isClosed() {
		return closed;
	}

	/* (non-Javadoc)
	 * @see com.sorma.querulous.session.Session#insert(T)
	 */
	@Override
	public <T> T insert(T entity) throws SQLException{
		Persister<T> persister = Persister.of( (Class<T>) entity.getClass());
		StatementPreParser preParser = getPreParser();
		persister.setPreParser(preParser);
		persister
			.using(connection)
			.insert(entity);
		return entity;
	}



	/* (non-Javadoc)
	 * @see com.sorma.querulous.session.Session#update(T)
	 */
	@Override
	public <T> T update(T entity) throws SQLException{
		StatementPreParser preParser = getPreParser();
		Persister<T> persister = Persister.of( (Class<T>) entity.getClass());
		persister.setPreParser(preParser);
		persister
			.using(connection)
			.update(entity);
		return entity;
	}


	/* (non-Javadoc)
	 * @see com.sorma.querulous.session.Session#query(java.lang.String)
	 */
	@Override
	public Query query(String statement) throws SQLException{
		Query qry = Query.query(preParse(statement))
			.using(connection);
		qry.setValueMapper(valueMapper);
		return qry;
	}

	private String preParse(String statement) throws SQLException {
		try {
			
			String actualStatement;
			StatementPreParser preParser = getPreParser();
			boolean doPreParse = preParser != null;
			if(doPreParse) {
				actualStatement = preParser.parse(statement);
				logger.debug("Updated statement \nfrom: \n"+statement+" \nTo:"+actualStatement);
			}else {
				actualStatement = statement;
			}
			return doPreParse ? preParser.parse(statement) : statement;
		}catch (Exception e) {
			throw new SQLException("Failed to pre-parse statement: "+statement,e);
		}
	}

	/* (non-Javadoc)
	 * @see com.sorma.querulous.session.Session#queryWithValues(java.lang.String, java.lang.String, java.util.Collection)
	 */
	@Override
	public Query queryWithValues(String statement, String listName , Collection values) throws SQLException {
		CollectionValuedStatement cstmt = new CollectionValuedStatement(statement);
		cstmt.paramList(listName, values);
		return query(cstmt.getStatement())
			.param(cstmt.getParams())
			.using(connection);

	}
	/* (non-Javadoc)
	 * @see com.sorma.querulous.session.Session#queryWithValues(java.lang.String, java.lang.String, java.util.Collection, java.lang.String, java.util.Collection)
	 */
	@Override
	public Query queryWithValues(String statement, String listName , Collection values,
			String listName2 , Collection values2
			) throws SQLException {
		CollectionValuedStatement cstmt = new CollectionValuedStatement(statement);
		cstmt.paramList(listName, values);
		cstmt.paramList(listName2, values2);
		return query(cstmt.getStatement())
			.param(cstmt.getParams())
			.using(connection);

	}

	/* (non-Javadoc)
	 * @see com.sorma.querulous.session.Session#buildFunctionsAPI(java.lang.Class)
	 * 
	 */
	/**
	 * @deprecated : use {@link Session#useAPI(Class)}
	 */
	@Override
	@Deprecated 
	public <T> T buildFunctionsAPI(Class<T> class1) throws  SQLException {
		try {
			return DBFunctionBuilder.buildAdapter(class1, connection);
		} catch (Exception e) {
			throw new SQLException("Failed to build DB function adapter for class "+class1,e);
		}

	}
	
	/* (non-Javadoc)
	 * @see com.sorma.querulous.session.Session#useAPI(java.lang.Class)
	 */
	@Override
	public <T> T useAPI(Class<T> clazz){
		return factory.getAPI(clazz, this);
	}

	public Connection getConnection() {
		return connection;
	}

	public void setPreParser(StatementPreParser preParser) {
		this.preParser = preParser;
	}

	public StatementPreParser getPreParser() {
		return preParser;
	}

	public EntityValueMapper getValueMapper() {
		return valueMapper;
	}

	public void setValueMapper(EntityValueMapper valueMapper) {
		this.valueMapper = valueMapper;
	}


	@Override
	public ParsedStatement parse(String statement) {
		String afterPreParse;
		if(preParser!=null) {
			afterPreParse = preParser.parse(statement);
		}else {
			afterPreParse = statement;
		}
		return new ParsedStatement(afterPreParse);
	}
	
	
}
