package com.sorma.querulous.session;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.log4j.Logger;

import com.sorma.querulous.DBFunctionBuilder;
import com.sorma.querulous.EntityValueMapper;
import com.sorma.querulous.Persister;
import com.sorma.querulous.Query;
import com.sorma.querulous.fun.AfterInsert;
import com.sorma.querulous.fun.DBFunctionFactory;
import com.sorma.querulous.helpers.CollectionValuedStatement;


public class SessionImpl extends AbstractSession {

	private static final AtomicInteger counter = new AtomicInteger(0);
	public final int id;
	public SessionImpl(Connection connection, CloseListener closeListener, Boolean autocommit, DBFunctionFactory factory) {
		super(connection, closeListener, autocommit, factory);
		id = counter.incrementAndGet();
		logger.debug("Starting session "+this);
	}

	private static Logger logger = Logger.getLogger(SessionImpl.class);

	@Override
	protected void finalize() throws Throwable {
		if(!connection.isClosed()){
			logger.warn(String.format("Session %s was not closed. Releasing connection %s",this,connection));
			try{
				if(!connection.getAutoCommit()){
					connection.rollback();
				}
			} finally {
				closeConnection();
			}
		}
	}

	@Override
	public String toString() {
		return "SessionImpl [id=" + id + ", connection=" + connection + "]";
	}


	
}
