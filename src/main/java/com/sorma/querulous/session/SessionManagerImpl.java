package com.sorma.querulous.session;

import java.lang.ref.WeakReference;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.Callable;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.sorma.querulous.DefaultValueMapper;
import com.sorma.querulous.EntityValueMapper;
import com.sorma.querulous.fun.DBFunctionFactory;

public class SessionManagerImpl implements SessionManager, CloseListener {

	private DataSource ds;
	private StatementPreParser preParser;
	private EntityValueMapper valueMapper = new DefaultValueMapper();

	


	private static Logger logger = Logger.getLogger(SessionManagerImpl.class);
	private boolean isShutDown = false;
	private DBFunctionFactory dbFunctionFactory = new DBFunctionFactory();

	private Set<WeakReference<SessionImpl>> openSessions = initSessionSet();

	private Set<WeakReference<SessionImpl>> initSessionSet() {
		return Collections.synchronizedSet(new HashSet<WeakReference<SessionImpl>>());
	}

	public SessionManagerImpl(DataSource ds) {
		super();
		this.ds = ds;
	}

	private ThreadLocal<WeakReference<SessionImpl>> currentSessionHolder = new ThreadLocal<WeakReference<SessionImpl>>();

	@Override
	public synchronized  Session currentSession() throws SQLException {
		WeakReference<SessionImpl> wref = currentSessionHolder.get();
		if(wref == null || wref.get() == null){
			wref = createThreadSession();
			openSessions.add(wref);
		}
		SessionImpl session =  wref.get();
		Connection connection = session.getConnection();
		if(connection.isClosed()) {
			logger.warn("Session "+session+ " is holding reference to a closed connection :"+connection) ;
			openSessions.remove(wref);
			wref = createThreadSession();
			openSessions.add(wref);
		}
		return session;
	}


	private Map<Connection, Boolean> connectionsAutocommitState = new WeakHashMap<Connection, Boolean>();

	private WeakReference<SessionImpl> createThreadSession() throws SQLException {
		WeakReference<SessionImpl> wref;
		Connection connection = getConnection();

		logger.debug("Acquired connection "+connection);

		Boolean autocommit = connectionsAutocommitState.get(connection);
		if(autocommit==null){
			autocommit = connection.getAutoCommit();
			logger.debug("Setting default autocommit for "+connection);
			connectionsAutocommitState.put(connection, autocommit);
		}
		SessionImpl session = new SessionImpl(connection,this,autocommit, dbFunctionFactory);
		session.setValueMapper(valueMapper);
		if(preParser!=null) {
			session.setPreParser(preParser);
		}
		wref = new WeakReference<SessionImpl>(session);
		currentSessionHolder.set(wref);
		return wref;
	}

	private Connection getConnection() throws SQLException {
		return ds.getConnection();
	}

	public synchronized void onClose(Session session){
		removeSession(session);
	}

	protected synchronized  void removeSession(Session session){
		Iterator<WeakReference<SessionImpl>> iterator = openSessions.iterator();
		while (iterator.hasNext()) {
			WeakReference<SessionImpl> wr = iterator.next();
			Session s = wr.get();
			if(s == null || s.equals(session)){
				iterator.remove();
			}
		}
		WeakReference<SessionImpl> wr  = currentSessionHolder.get();
		// This method can be called by another thread, in that case
		// the session holder can be either null or hold a different session
		if(wr!=null) {
			Session s = wr.get();
			// Remove session from holder only if session is the same
			if(s == null || s.equals(session)){
				currentSessionHolder.set(null);
			}
		}

	}

	public void dumpCurrentSessions(){
		logger.info("List open sessions: ");
		Iterator<WeakReference<SessionImpl>> iterator = openSessions.iterator();
		while (iterator.hasNext()) {
			WeakReference<SessionImpl> weakReference = iterator
					.next();
			Session session = weakReference.get();
			if(session!=null){
				logger.info("Session "+session+" is active.");
			}
		}
	}

	private Callable<Void> shutDownHandler;
	public Callable<Void> getShutDownHandler() {
		return shutDownHandler;
	}

	public void setShutDownHandler(Callable<Void> shutDownHandler) {
		this.shutDownHandler = shutDownHandler;
	}

	private WeakReference<DataSource> dsRef;
	
	@Override
	public void shutDown() {
		isShutDown=true;
		try {
			if(shutDownHandler!=null){
				shutDownHandler.call();
			}
		} catch (Exception e) {
			logger.error("Error shutting down "+ds);
		} finally {
			// Keep a weak reference of the datasource.
			// It should die soon, if not, we have something
			// to inspect.
			dsRef = new WeakReference<DataSource>(ds);
			ds = null;
		}
	}

	@Override
	protected void finalize() throws Throwable {
		if(!isShutDown){
			try{
				shutDown();
			} catch(Throwable e){
				// SWALLOW
				logger.error("Error in "+this.getClass()+" finalization ",e);
			}
			
		}
	}
	public StatementPreParser getPreParser() {
		return preParser;
	}

	public void setPreParser(StatementPreParser preParser) {
		this.preParser = preParser;
	}
	public EntityValueMapper getValueMapper() {
		return valueMapper;
	}

	public void setValueMapper(EntityValueMapper valueMapper) {
		this.valueMapper = valueMapper;
	}
	
}
