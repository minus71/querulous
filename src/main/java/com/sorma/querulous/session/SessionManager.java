package com.sorma.querulous.session;

import java.sql.SQLException;

public interface SessionManager {
	
	Session currentSession() throws SQLException;
	void shutDown();
}
