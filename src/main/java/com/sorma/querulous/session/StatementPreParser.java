package com.sorma.querulous.session;

public interface StatementPreParser {

	String parse(String statement) ;
}
