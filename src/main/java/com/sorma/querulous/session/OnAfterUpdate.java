package com.sorma.querulous.session;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public interface OnAfterUpdate<T> {
	T apply(PreparedStatement stmt, Integer updateCount) throws SQLException;
}
