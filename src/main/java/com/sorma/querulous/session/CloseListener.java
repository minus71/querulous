package com.sorma.querulous.session;

public interface CloseListener {

	void onClose(Session session);

}
