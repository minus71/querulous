package com.sorma.querulous;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MultiResult {
	private Integer value;
	private List<List<Map<String,Object>>> results = new ArrayList<List<Map<String,Object>>>();

	public Integer getValue() {
		return value;
	}
	public void setValue(Integer value) {
		this.value = value;
	}
	public List<List<Map<String, Object>>> getResults() {
		return results;
	}
	public void setResults(List<List<Map<String, Object>>> results) {
		this.results = results;
	}
}
