package com.sorma.querulous;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.google.common.base.Function;
import com.sorma.querulous.fun.Binder;
import com.sorma.querulous.helpers.DBHelpers;
import com.sorma.querulous.perf.PerformanceEvent;
import com.sorma.querulous.perf.PerformanceLogger;
import com.sorma.querulous.session.OnAfterUpdate;
import com.sorma.querulous.session.StatementPreParser;

public class Query extends Connectable {

	private String sourceStatement;
	private static Logger logger = Logger.getLogger(Query.class);
	private EntityValueMapper valueMapper = new DefaultValueMapper();
	private Map<String, Object> parameters = new LinkedHashMap<String, Object>();
	private ParsedStatement parsedStatement;
	private PerformanceLogger perfLog = PerformanceLogger.getInstance();

	public EntityValueMapper getValueMapper() {
		return valueMapper;
	}

	public void setValueMapper(EntityValueMapper valueMapper) {
		this.valueMapper = valueMapper;
	}


	public class QueryId {
		@SuppressWarnings("rawtypes")
		private List values = new ArrayList();
		private String query;

		public QueryId(String query) {
			super();
			this.query = query;
		}

		@Override
		public int hashCode() {
			return query.hashCode();
		}

		public void add(Object value) {
			values.add(value);
		}

		@Override
		public boolean equals(Object obj) {
			if (obj == null || query == null)
				return false;
			if (obj instanceof QueryId) {
				QueryId other = (QueryId) obj;
				if (!query.equals(other.query)) {
					if (values.size() != other.values.size())
						return false;
					for (int i = 0; i < values.size(); i++) {
						Object val = values.get(i);
						Object otherVal = other.values.get(i);
						if (val == null && otherVal != null) {
							return false;
						} else {
							if (!val.equals(otherVal))
								return false;
						}
					}
					return true;
				}
			} else {
				return false;
			}
			return super.equals(obj);
		}

	}


	public Query(String statement) {
		this.sourceStatement = statement;
		parsedStatement = new ParsedStatement(statement);
	}

	
	
	private Query(String sourceStatement, EntityValueMapper valueMapper, Map<String, Object> parameters,
			ParsedStatement parsedStatement) {
		super();
		this.sourceStatement = sourceStatement;
		this.valueMapper = valueMapper;
		this.parameters = parameters;
		this.parsedStatement = parsedStatement;
	}

	public Query(String statement, StatementPreParser statementPreParser) {
		String preparsed = statementPreParser.parse(statement);
		parsedStatement = new ParsedStatement(preparsed);
	}

	public Query using(Connection connection) {
		use(connection);
		return this;
	}

	public Query using(String jndiName) {
		use(jndiName);
		return this;
	}

	public Query commitOnClose() {
		setCommitOnClose(true);
		return this;
	}

	public Query using(DataSource ds) {
		use(ds);
		return this;
	}

	public Query param(String name, Object value) {
		parameters.put(name, value);
		return this;
	}

	public Query param(Map<String, Object> params) {
		parameters.putAll(params);
		return this;
	}

	
	public <T> CachedQuery<T> cachedList(Function<ResultSet, T> handler){
		return new CachedQuery<T>(this, handler);
	}
	
	
	public <T> List<T> list(Function<ResultSet, T> handler) throws SQLException {
		List<T> results = new ArrayList<T>();
		logger.debug("Preparing callable statement: " + parsedStatement);
		Connection conn = getConnection();
		try {

			PreparedStatement stmt = conn.prepareStatement(parsedStatement.getSqlStatement(),ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY );
			PerformanceEvent evt = onBeginQuery(stmt);
			try {

				// checkCacheable();

				bind(stmt, parameters);
				ResultSet resultSet = stmt.executeQuery();
				try {
					int recordsRead = 0;
					while (resultSet.next()) {
						T res = handler.apply(resultSet);
						results.add(res);
						recordsRead++;
					}
					evt.setAttr("READ", recordsRead);
				} finally {
					DBHelpers.safeClose(resultSet);
				}
			}catch (Exception e) {
				throw new SQLException("Query failed: " + parsedStatement, e);
			} finally {
				DBHelpers.safeClose(stmt);
				evt.stop();
			}
			return results;
		} finally {
			autoClose();
		}
	}

	private static final OnAfterUpdate<Integer> countUpdates = new OnAfterUpdate<Integer>() {
		
		@Override
		public Integer apply(PreparedStatement stmt, Integer updateResult) throws SQLException {
			
			return updateResult;
		}
	};
	
	public int update() throws SQLException {
		return update(countUpdates);
	}
	
	public <T> T update(OnAfterUpdate<T> onAfterUpdate) throws SQLException {
		T result;
		Connection conn = getConnection();
		try {
			logger.debug("Preparing callable statement: " + parsedStatement);
			PreparedStatement stmt = conn.prepareStatement(parsedStatement.getSqlStatement());
			PerformanceEvent evt = onBeginQuery(stmt);

			try {
				bind(stmt, parameters);
				int rowCount = stmt.executeUpdate();
				result = onAfterUpdate.apply(stmt, rowCount);
				evt.setAttr("Record Count", rowCount);
				
			} catch (SQLException e) {
				throw new SQLException("Query failed: " + parsedStatement, e);
			} finally {
				DBHelpers.safeClose(stmt);
				evt.stop();
			}
		} finally {
			autoClose();
		}

		return result;
	}


	public Integer call() throws SQLException {
		try {
			Integer result = null;
			logger.debug("Preparing callable statement: " + parsedStatement);

			String sqlStatement = parsedStatement.getSqlStatement();

			CallableStatement cstmt = getConnection().prepareCall(sqlStatement);
			PerformanceEvent evt = onBeginQuery(cstmt);

			try {
				bind(cstmt, parameters);
				cstmt.registerOutParameter(1, Types.INTEGER);
				cstmt.execute();
				result = cstmt.getInt(1);
				evt.setAttr("Record Count", 1);
			} catch (Exception e) {
				throw new SQLException("Query failed: " + parsedStatement, e);
			} finally {
				autoClose();
				evt.stop();
			}

			return result;
		} finally {
			autoClose();
		}
	}

	protected void checkCacheable() {
		QueryId queryId = createQueryId();

		Integer called = queryStats.get(queryId);
		if (called == null) {
			called = 0;
		}
		queryStats.put(queryId, called + 1);

		if (called > 10) {
			logger.warn("Optimizable query " + this);
		}
	}

	protected QueryId createQueryId() {
		QueryId queryId = new QueryId(this.parsedStatement.getSqlStatement());
		for (String paramName : parsedStatement.getParametersList()) {
			Object value = parameters.get(paramName);
			queryId.add(value);
		}
		return queryId;
	}

	protected PerformanceEvent onBeginQuery(PreparedStatement stmt) {
		PerformanceEvent evt = perfLog.onBegin();
		evt.setName("QUERY");
		evt.setAttr("STMT", parsedStatement.getSqlStatement());
		evt.setAttr("PARAMS", parameters.toString());
		return evt;
	}

	private static Map<QueryId, Integer> queryStats = Collections
			.synchronizedMap(new HashMap<Query.QueryId, Integer>());

	protected void bind(PreparedStatement stmt, Map<String, Object> paramsMap) throws SQLException {
		Binder.bind(stmt, paramsMap, parsedStatement.getParametersList());
	}

	public static Query query(String statement) {
		return new Query(statement);
	}
	
	
	public CachedQuery<Map<String, Object>> cachedListRecords(){
		Function<ResultSet, Map<String, Object>> handler = new ResultSetToMap();
		return new CachedQuery<Map<String, Object>>(this, handler);
	}


	public List<Map<String, Object>> listRecords() throws SQLException {
		return list(new ResultSetToMap());
	}

	public <T> CachedQuery<T> cachedListEntities(Class<T> entityClass) throws SQLException {
		ResultSetToEntity<T> handler = new ResultSetToEntity<T>(entityClass);
		handler.setEntityValueMapper(valueMapper);
		return new CachedQuery<T>(this, handler);
	}

	
	public <T> List<T> listEntities(Class<T> entityClass) throws SQLException {
		ResultSetToEntity<T> handler = new ResultSetToEntity<T>(entityClass);
		handler.setEntityValueMapper(valueMapper);
		return list(handler);
	}

	public <T> T one(Class<T> clazz) throws SQLException {
		List<T> lst = listEntities(clazz);
		return (lst == null || lst.isEmpty()) ? null : lst.get(0);
	}

	public <T> T only(Class<T> clazz) throws SQLException {
		List<T> lst = listEntities(clazz);
		return (lst == null || lst.size() != 1) ? null : lst.get(0);
	}

	/**
	 * 
	 * Executes the query and returns an iterator which iterates a {@link ResultSet}
	 * of type <strong>TYPE_FORWARD_ONLY</strong> with concurrency of type
	 * <strong>CONCUR_READ_ONLY</strong>. <br>
	 * The {@link QueryIterator} MUST be either consumed completely or closed.<br>
	 * <br>
	 * Performance tracking has a special attribute:<br>
	 * <strong>EXECUTE_QUERY_TIME</strong> shows the time in ms it took to execute
	 * the statement.<br>
	 * It is a more accurate time to measure query performance because many
	 * operation and even other queries can be run while this is operator is kept
	 * unclosed
	 * 
	 * @param <T>
	 * @param handler - The mapping function which transforms the record into type
	 *                <T>
	 * @return A {@link QueryIterator} which is a closable iterator.
	 * @throws SQLException
	 */
	public <T> QueryIterator<T> iterate(Function<ResultSet, T> handler) throws SQLException {
		logger.debug("Preparing callable statement: " + parsedStatement);
		Connection conn = getConnection();
		final int resultsetConcurrency = ResultSet.CONCUR_READ_ONLY;
		final int resultsetType = ResultSet.TYPE_FORWARD_ONLY;

		final PreparedStatement stmt = conn.prepareStatement(parsedStatement.getSqlStatement(), resultsetType,
				resultsetConcurrency);
		final PerformanceEvent evt = onBeginQuery(stmt);
		try {

			// checkCacheable();
			bind(stmt, parameters);
			long t0 = evt.getEventStart().getTime();
			long t1 = System.currentTimeMillis();
			ResultSet resultSet = stmt.executeQuery();
			// EXECUTE_QUERY_TIME shows the time in ms it took to execute the statement
			// It is a more accurate time to measure query performance because
			// many operation and even other queries can be run while this is operator is
			// kept unclosed
			evt.setAttr("EXECUTE_QUERY_MS", t1 - t0);

			final AtomicReference<ResultsetIterator<T>> iteratorRef = new AtomicReference<ResultsetIterator<T>>();
			final ResultsetIterator<T> iterator = new ResultsetIterator<T>(resultSet, handler, new Runnable() {
				@Override
				public void run() {
					DBHelpers.safeClose(stmt);
					autoClose();
					ResultsetIterator<T> resultsetIterator = iteratorRef.get();
					if (resultsetIterator != null) {
						evt.setAttr("READ", resultsetIterator.getRecordCounter());
						evt.stop();
					}
				}
			});
			iteratorRef.set(iterator);
			return iterator;
		} catch (Exception e) {
			throw new SQLException("Query failed: " + parsedStatement, e);
		} finally {
//			DBHelpers.safeClose(stmt);
		}
	}
	public Map<String, Object> getParameters() {
		return parameters;
	}

	public Query with(StatementPreParser statementPreParser) {
		String preParsed = statementPreParser.parse(sourceStatement);
		ParsedStatement parsedStatement = new ParsedStatement(preParsed);
		Query that = new Query(sourceStatement, valueMapper, parameters, parsedStatement);
		that.connectAs(this);
		return that;
	}
	
	public Query with(EntityValueMapper entityValueMapper) {
		Query that = new Query(sourceStatement, entityValueMapper, parameters, parsedStatement);
		that.connectAs(this);
		return that;
	}
}
