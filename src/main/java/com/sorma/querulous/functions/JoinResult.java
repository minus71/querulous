package com.sorma.querulous.functions;

public class JoinResult<S,J> {

	private Class<S> selectClass;
	private Class<J> joinClass;

	private S selectResult;
	private J joinResults;
	
	
	public JoinResult(Class<S> selectClass, Class<J> joinClass) {
		super();
		this.selectClass = selectClass;
		this.joinClass = joinClass;
	}

	public Class<S> getSelectClass() {
		return selectClass;
	}

	public Class<J> getJoinClass() {
		return joinClass;
	}

	public S getSelectResult() {
		return selectResult;
	}

	public void setSelectResult(S selectResult) {
		this.selectResult = selectResult;
	}



	public J getJoinResults() {
		return joinResults;
	}

	public void setJoinResults(J joinResults) {
		this.joinResults = joinResults;
	}

	@Override
	public String toString() {
		return "JoinResult [selectClass=" + selectClass + ", joinClass=" + joinClass + ", selectResult=" + selectResult
				+ ", joinResults=" + joinResults + "]";
	}

	
	
	
}
