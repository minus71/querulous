package com.sorma.querulous.functions;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.sorma.querulous.session.OnAfterUpdate;

public class GeneratedKeys implements OnAfterUpdate<List<Map<String, Object>>> {
	
	@Override
	public List<Map<String, Object>> apply(PreparedStatement stmt, Integer updateCount) throws SQLException {
		List<Map<String, Object>> result = new ArrayList<Map<String,Object>>();
		ResultSet generatedKeys = stmt.getGeneratedKeys();
		ResultSetMetaData metaData = generatedKeys.getMetaData();
		int columnCount = metaData.getColumnCount();
		Map<String, Object> keys = new LinkedHashMap<String, Object>();
		while (generatedKeys.next()) {
			for (int i = 1; i <= columnCount; i++) {
				Object value = generatedKeys.getObject(i);
				String columnName = metaData.getColumnName(i);
				keys.put(columnName, value);
			}
			result.add(keys);
		}
		return result;
	}
}
