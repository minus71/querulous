package com.sorma.querulous;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.google.common.base.Joiner;
import com.sorma.querulous.annotation.Auto;
import com.sorma.querulous.annotation.Column;
import com.sorma.querulous.annotation.DB;
import com.sorma.querulous.annotation.ID;
import com.sorma.querulous.annotation.Schema;
import com.sorma.querulous.annotation.TableName;
import com.sorma.querulous.fun.AfterInsert;
import com.sorma.querulous.fun.Binder;
import com.sorma.querulous.fun.TypedNullValue;
import com.sorma.querulous.helpers.DBHelpers;
import com.sorma.querulous.perf.PerformanceEvent;
import com.sorma.querulous.perf.PerformanceLogger;
import com.sorma.querulous.session.StatementPreParser;

public class Persister<T> extends Connectable {

	private static Logger logger = Logger.getLogger(Persister.class);

	private Class<T> entityClass;
	private StatementPreParser preParser;

	public StatementPreParser getPreParser() {
		return preParser;
	}

	public void setPreParser(StatementPreParser preParser) {
		this.preParser = preParser;
	}

	public Persister(Class<T> entityClass) {
		this.entityClass = entityClass;
	}

	public static <T> Persister<T> of(Class<T> clazz) {
		return new Persister<T>(clazz);
	}

	public Persister<T> using(Connection connection) {
		use(connection);
		return this;
	}

	public Persister<T> using(String jndiName) {
		use(jndiName);
		return this;
	}

	public Persister<T> using(DataSource ds) {
		use(ds);
		return this;
	}

	public Persister<T> commitOnClose(){
		setCommitOnClose(true);
		return this;
	}


	public void update(T entity) throws SQLException {
		try {
			Connection conn = getConnection();

			String tableName = getTableName();

			Map<String, Object> fieldsMap = getFieldsMapping(entity);
			Set<String> idSet = getIdSet();

			List<String> whereConditions = new ArrayList<String>();
			List<String> setConditions = new ArrayList<String>();
			for (String fieldName : fieldsMap.keySet()) {
				String fmr = String.format("%s = :%s",fieldName,fieldName);
				if (idSet.contains(fieldName)) {
					whereConditions.add(fmr);
					idSet.remove(fmr);
				}else{
					setConditions.add(fmr);
				}
			}

			String setConditionsString = Joiner.on(",").join(setConditions);
			String whereConditionsString = Joiner.on(" AND ").join(whereConditions);



			String statement = String.format("UPDATE %s SET %s WHERE %s", tableName, setConditionsString, whereConditionsString);
			statement = preParse(statement);

			
			PreparedStatement stmt = null;
			ParsedStatement parsedStatement = new ParsedStatement(statement);
			String sqlStatement = parsedStatement.getSqlStatement();
			try {
				PerformanceEvent evt = onBeginQuery(parsedStatement,fieldsMap);
				stmt = conn.prepareStatement(sqlStatement);
				Binder.bind(stmt, fieldsMap, parsedStatement.getParametersList());
				stmt.executeUpdate();
				evt.stop();
			} finally {
				DBHelpers.safeClose(stmt);
			}

		} finally {
			autoClose();
		}
	}
	
	

	public void insert(T entity) throws SQLException {
		innerInsert(entity);
	}
	
	private void innerInsert(T entity) throws SQLException {
		try {
			Connection conn = getConnection();

			String tableName = getTableName();

			Map<String, Object> fieldsMap = getFieldsMapping(entity);
			Map<String,Field> autoMap = getAutoMap();
			Set<String> autoSet = autoMap.keySet();
			for (String auto : autoSet) {
				fieldsMap.remove(auto);
			}
			

			String columns = Joiner.on(",").join(fieldsMap.keySet());
			String parameters = ":" + Joiner.on(",:").join(fieldsMap.keySet());

			String statement = String.format("INSERT INTO %s (%s) VALUES (%s)", tableName, columns, parameters);

			statement = preParse(statement);
			
			
			ParsedStatement parsedStatement = new ParsedStatement(statement);
			String sqlStatement = parsedStatement.getSqlStatement();

			PreparedStatement stmt = null;
			try {
				PerformanceEvent evt = onBeginQuery(parsedStatement,fieldsMap);
				stmt = conn.prepareStatement(sqlStatement, Statement.RETURN_GENERATED_KEYS);
				Binder.bind(stmt, fieldsMap, parsedStatement.getParametersList());
				
				int n = stmt.executeUpdate();
				if(n>0) {
					try {
						updateEntity(entity, autoMap, stmt);
					} catch (Exception e) {
						logger.warn("Failed to update entity " + entity + " after insert",e);
					}
				}
				
				evt.stop();
			} finally {
				DBHelpers.safeClose(stmt);
			}

		} finally {
			autoClose();
		}
	}
	
	
	private String preParse(String statement) throws SQLException {
		try {
			
			String actualStatement;
			boolean doPreParse = preParser!=null;
			if(doPreParse) {
				actualStatement = preParser.parse(statement);
				logger.debug("Updated statement \nfrom: \n"+statement+" \nTo:"+actualStatement);
			}else {
				actualStatement = statement;
			}
			return doPreParse ? preParser.parse(statement) : statement;
		}catch (Exception e) {
			throw new SQLException("Failed to pre-parse statement: "+statement,e);
		}
	}


	private void updateEntity(T entity, Map<String,Field> autoMap, PreparedStatement stmt) throws SQLException {
		ResultSet res = null;
		try {
			res = stmt.getGeneratedKeys();
			
			if(res.next()) {
				for (Field  fld: autoMap.values()) {
					try {
						// Field fld = entityClass.getDeclaredField(string);
						fld.setAccessible(true);
						Object v = DBHelpers.getAssignableValue(res, 1, fld.getType());
						fld.set(entity, v);
						fld.setAccessible(false);
					} catch (Exception e) {
						logger.error("Error updating id for "+entity,e);
					}
				}
			}
		} finally {
			DBHelpers.safeClose(res);
		}
	}

	private Map<String, Object> getFieldsMapping(T entity) {
		Field[] declaredFields = entityClass.getDeclaredFields();

		Map<String, Object> fieldsMap = new LinkedHashMap<String, Object>();

		for (Field field : declaredFields) {

			
			int modifiers = field.getModifiers();
			boolean isStatic = Modifier.isStatic(modifiers);
			boolean isFinal = Modifier.isFinal(modifiers);
			boolean isTransient = Modifier.isTransient(modifiers);
			// boolean isAuto = field.isAnnotationPresent(Auto.class);
			
			if (!isStatic && !isFinal && !isTransient ) {
				addField(entity, fieldsMap, field);
			}

		}
		return fieldsMap;
	}

	private Set<String> getIdSet() {
		Field[] declaredFields = entityClass.getDeclaredFields();

		Set<String> ids = new LinkedHashSet<String>();

		for (Field field : declaredFields) {

			ID idAnnot = field.getAnnotation(ID.class);
			if(idAnnot!=null){
				ids.add(getFieldName(field));
			}
		}
		return ids;
	}
	
	private Map<String,Field> getAutoMap() {
		Field[] declaredFields = entityClass.getDeclaredFields();

		Map<String,Field> ids = new LinkedHashMap<String,Field>();

		for (Field field : declaredFields) {

			boolean isAuto = field.isAnnotationPresent(Auto.class);
			if(isAuto){
				ids.put(getFieldName(field),field);
			}
		}
		return ids;
	}


	private void addField(T entity, Map<String, Object> fieldsMap, Field field) {
		String name = getFieldName(field);
		field.setAccessible(true);
		Object value;
		try {
			value = field.get(entity);
			if(value==null) {
				fieldsMap.put(name, TypedNullValue.from(field.getType()));
			}else {
				fieldsMap.put(name, value);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			field.setAccessible(false);
		}
	}

	private String getFieldName(Field field) {
		Column annotCol = field.getAnnotation(Column.class);
		String name = (annotCol == null) ? field.getName() : annotCol.value();
		return name;
	}

	private String getTableName() {
		Schema schemaAnnot = entityClass.getAnnotation(Schema.class);
		DB dbAnnot = entityClass.getAnnotation(DB.class);
		String dbPrefix = (dbAnnot == null) ? "" : dbAnnot.value() + ".";
		String schemaPrefix = (schemaAnnot == null) ? "" : schemaAnnot.value() + ".";

		TableName tabAnnotation = entityClass.getAnnotation(TableName.class);

		String tableName = dbPrefix + schemaPrefix
				+ ((tabAnnotation == null) ? entityClass.getSimpleName() : tabAnnotation.value());
		return tableName;
	}
	
	PerformanceLogger perfLog = PerformanceLogger.getInstance();
	protected PerformanceEvent onBeginQuery(ParsedStatement parsedStatement, Object parameters) {
		PerformanceEvent evt = perfLog.onBegin();
		evt.setName("QUERY");
		evt.setAttr("STMT", parsedStatement.getSqlStatement());
		evt.setAttr("PARAMS", parameters.toString());
		return evt;
	}

}
