package com.sorma.querulous;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.common.base.Function;


public class ResultSetToMap implements Function<ResultSet, Map<String, Object>> {
	ResultSetMetaData metaData;
	Map<Integer, String> cachedNames = new LinkedHashMap<Integer, String>();
	Integer numColumns;
	
	@Override
	public Map<String, Object> apply(ResultSet input) {
		readMeta(input);
		Map<String, Object> record = new LinkedHashMap<String,Object>();
		try {
			int columnCount = getColumns();
			for (int i = 1; i <= columnCount; i++) {
				Object value = input.getObject(i);
				record.put(getName(i), value);
			}
		} catch (SQLException e) {
			throw new RuntimeException("Failed mapping resultet.",e);
		}
		return record;
	}

	protected void readMeta(ResultSet input) {
		if(metaData==null){
			try {
				metaData = input.getMetaData();
				numColumns = metaData.getColumnCount();
			} catch (SQLException e) {
				throw new RuntimeException("Failed obtaing metadata.",e);
			}
		}
	}

	protected String getName(int i) throws SQLException {
		String name = cachedNames.get(i);
		if(name==null){
			name = metaData.getColumnName(i);
			cachedNames.put(i, name);
		}
		return name;
	}

	protected int getColumns() throws SQLException {
		return numColumns;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((numColumns == null) ? 0 : numColumns.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}
	
	

}
