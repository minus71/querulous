package com.sorma.querulous;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import com.google.common.base.Function;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;


/**
 * @author minus
 *
 * @param <T>
 */
public class CachedQuery<T> {

	
	final Query  query;
	final Function<ResultSet, T> handler;
	
	private Map<String, Object> parameters = new LinkedHashMap<String, Object>();
	
	

	public CachedQuery(Query query, Function<ResultSet, T> handler) {
		super();
		this.query = query;
		this.handler = handler;
	}

	public CachedQuery<T> param(String name, Object value) {
		parameters.put(name, value);
		return this;
	}

	public CachedQuery<T> param(Map<String, Object> params) {
		parameters.putAll(params);
		return this;
	}

	LoadingCache<ParamHolder, List<T>> listEntityCache = CacheBuilder.newBuilder().build(new CacheLoader<ParamHolder, List<T>>(){
		@Override
		public List<T> load(ParamHolder key) throws Exception {
			query.param(parameters);
			List<T> results = query.list(handler);
			return results;
		}
	}); 
	
	public List<T> list() throws SQLException {
		try {
			return (List<T>) listEntityCache.get(new ParamHolder(parameters));
		} catch (ExecutionException e) {
			Throwable cause = e.getCause();
			if(cause instanceof SQLException) {
				throw (SQLException)cause;
			}else {
				if(cause!=null) {
					throw (SQLException)new SQLException(cause);
				} else {
					throw (SQLException)new SQLException(e);
				}
			}
		}
	}
	
	
	private static class ParamHolder{
		private Map<String, Object> parameters = new LinkedHashMap<String, Object>();

		public ParamHolder(Map<String, Object> parameters) {
			this.parameters = Collections.unmodifiableMap(parameters);
		}

		@Override
		public int hashCode() {
			return parameters.hashCode();
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			ParamHolder other = (ParamHolder) obj;
			if (parameters == null) {
				if (other.parameters != null)
					return false;
			} else if (!parameters.equals(other.parameters))
				return false;
			return true;
		}
	}

	
	/**
	 * Clear the cache from all entries
	 * 
	 */
	public void evict() {
		listEntityCache.invalidateAll();
	}
	
}
