package com.sorma.querulous;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

import com.google.common.base.Function;
import com.sorma.querulous.helpers.DBHelpers;

public class ResultsetIterator<T> implements QueryIterator<T> {

	private final Logger logger = Logger.getLogger(getClass());
	private final ResultSet resultSet;
	private final Function<ResultSet, T> mapperFn;
	private final Runnable oncloseFn;
	private final String trace;
	private boolean hasRecord = false;
	private boolean closed = false;
	private int recordCounter = 0;
	
	

	public ResultsetIterator(ResultSet resultSet, Function<ResultSet, T> mapperFn, Runnable oncloseFn) {
		super();
		this.resultSet = resultSet;
		this.mapperFn = mapperFn;
		this.oncloseFn = oncloseFn;
		trace = getTrace();
	}

	public int getRecordCounter() {
		return recordCounter;
	}

	public void setRecordCounter(int recordCounter) {
		this.recordCounter = recordCounter;
	}

	@Override
	public boolean hasNext() {
		boolean ok = false;
		if(!hasRecord) {
			try {
				hasRecord = resultSet.next();
				if(!hasRecord) {
					close();
				}
				ok = true;
			} catch (SQLException e) {
				hasRecord = false;
				throw new RuntimeSQLException(e);
			} catch (Throwable e) {
				hasRecord = false;
				if (e instanceof RuntimeException) {
					RuntimeException runtimeEx = (RuntimeException) e;
					throw runtimeEx;
				} if (e instanceof Error) {
					throw (Error) e;					
				}else {
					// Should not happen.
					throw new RuntimeException(e);
				}
			} finally {
				// Any exception will close the resultset
				if(!ok) {
					close();
				}
			}
		}
		return hasRecord;
	}

	@Override
	public T next() {
		if(hasRecord) {
			if(resultSet==null) {
				throw new NoSuchElementException();
			}
			T apply = mapperFn.apply(resultSet);
			recordCounter++;
			// Current record consumed
			hasRecord = false;
			return apply;
		} else {
			if(!closed) {
				hasNext();
				if(hasRecord) {
					return next();
				}
			}
			throw new NoSuchElementException();
		}
	}

	@Override
	protected void finalize() throws Throwable {
		if(!closed) {
			logger.warn("Resultset not closed\n\tCalled at: "+trace);
		}
		close();
	}

	private String getTrace() {
		String trace = "NO-TRACE";
		StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
		for (StackTraceElement stackTraceElement : stackTrace) {
			String className = stackTraceElement.getClassName();
			if(!className.startsWith("com.sorma.querulous")) {
				String clazz = stackTraceElement.getClassName();
				String _fileName = stackTraceElement.getFileName();
				String fileName = _fileName == null ? "n/a" : _fileName;
				String methodName = stackTraceElement.getMethodName();
				String lineNumber = stackTraceElement.getLineNumber() >= 0 ? ""+stackTraceElement.getLineNumber(): "n/a";
				
				trace = String.format("%s::%s (%s : [%s])", clazz, methodName, fileName, lineNumber);
				break;
			}
		}
		return trace;
	}


	@Override
	public void close() {
		if(!closed) {
			closed = true;
			hasRecord = false;
			DBHelpers.safeClose(resultSet);
			oncloseFn.run();
		}
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}
	
	
}
