package com.sorma.querulous;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

public class DefaultValueMapper implements EntityValueMapper {

	@Override
	public Object getValue(ResultSet input, Field field, String colName) {
		Object value=null;
		try{
			Class<?> type = field.getType();
			if(type.isAssignableFrom(String.class)){
				value = input.getString(colName);
			}else if (type.isAssignableFrom(Double.class)) {
				value = input.getDouble(colName);
			}else if (type.isAssignableFrom(Float.class)) {
				value = input.getFloat(colName);
			}else if (type.isAssignableFrom(Date.class)) {
				Timestamp timestamp = input.getTimestamp(colName);
				value = timestamp;
			}else if (type.isAssignableFrom(Integer.class)){
				value = input.getInt(colName);
			}else if (type.isAssignableFrom(Long.class)){
				value = input.getLong(colName);
			}else if (type.isAssignableFrom(Boolean.class)){
				value = input.getBoolean(colName);
			}else if (type.isAssignableFrom(BigDecimal.class)){
				value = input.getBigDecimal(colName);
			}else if (type.isAssignableFrom(byte[].class)){
				value = input.getBytes(colName);
			}else {
				Object dbMappedObject = input.getObject(colName);
				if(type.isInstance(dbMappedObject)) {
					value = dbMappedObject;
				}
				// Skip
			}
		}catch (SQLException e){
			String className = field!=null ? field.getDeclaringClass().getName(): null;
			throw new RuntimeException("Column "+colName+" of cannot be loaded into  "+className,e);
		}catch (Exception e) {
			String className = field!=null ? field.getDeclaringClass().getName(): null;
			throw new RuntimeException("Column "+colName+" of cannot be loaded into  "+className,e);
		}
		return value;
	}
	
	@Override
	public int hashCode() {
		return 1;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj==null) {
			return false;
		}
		return this.getClass().equals(obj.getClass());
	}

}
