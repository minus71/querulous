package com.sorma.querulous;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.google.common.base.Function;
import com.sorma.querulous.annotation.Column;
import com.sorma.querulous.annotation.MapValue;
import com.sorma.querulous.functions.JoinResult;


public class ResultSetToJoinEntity<T,J> implements Function<ResultSet, JoinResult<T, J>> {


	public final Class<T> entityClass;
	public final Class<J> joinClass;
	private EntityValueMapper entityValueMapper = new DefaultValueMapper();
	private ResultSetToEntity<T> entityMapper;
	private ResultSetToEntity<J> joinMapper;




	public ResultSetToJoinEntity(Class<T> entityClass, Class<J> joinClass) {
		super();
		this.entityClass = entityClass;
		this.joinClass= joinClass;
		entityMapper = new ResultSetToEntity<T>(entityClass);
		joinMapper = new ResultSetToEntity<J>(joinClass);
	}
	
	
	

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public JoinResult<T, J> apply(ResultSet input) {
		JoinResult<T, J> joinResult = new JoinResult<T, J>(entityClass, joinClass);
		T main = entityMapper.apply(input);
		J join= joinMapper.apply(input);
		joinResult.setSelectResult(main);
		joinResult.setJoinResults(join);
		return joinResult;
	}

	protected boolean has(int fieldModifiers, int modifierValue) {
		return (fieldModifiers & modifierValue) == modifierValue;
	}

	protected void setField(T entity, Field field, Object value) {
		try {
			field.set(entity, value);
		} catch (Exception e) {
			throw new RuntimeException("field "+field.getName()+" of class " +entityClass
					+ " cannot be set.");
		}
	}
}
