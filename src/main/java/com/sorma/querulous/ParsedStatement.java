package com.sorma.querulous;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParsedStatement {
	private String source;
	private String sqlStatement;
	private List<String> parametersList = new ArrayList<String>();


	public List<String> getParametersList() {
		return parametersList;
	}

	private static final Pattern rxParam = Pattern.compile("[^\\\\](:([a-zA-Z0-9_]*))");
	private static final Pattern rxUParam = Pattern.compile("\\\\:");

	public ParsedStatement(String stmt){
		source=stmt;
		sqlStatement = stmt;
		parseParameters();
		unescapeParameters();
	}

	protected void parseParameters() {
		Matcher m = rxParam.matcher(sqlStatement);
		while(m.find()){
			parametersList.add(m.group(2)) ;
			int left = m.start(1);
			int right = m.end(1);
			sqlStatement = sqlStatement.substring(0,left)+"?"+sqlStatement.substring(right);
			m = rxParam.matcher(sqlStatement);
		}
	}
	protected void unescapeParameters(){
		Matcher m = rxUParam.matcher(sqlStatement);
		if(m.find()){
			sqlStatement = m.replaceAll(":");
		}
	}

	public String getSource() {
		return source;
	}

	public String getSqlStatement() {
		return sqlStatement;
	}

	@Override
	public String toString() {

		return "ParsedStatement [source=" + source + "]";
	}



}
