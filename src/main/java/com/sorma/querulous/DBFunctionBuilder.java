package com.sorma.querulous;

import static com.sorma.querulous.helpers.DBHelpers.safeClose;
import static java.lang.String.format;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.sorma.querulous.annotation.Adapter;
import com.sorma.querulous.annotation.DB;
import com.sorma.querulous.annotation.Schema;
import com.sorma.querulous.fun.DBFunctionMeta;
import com.sorma.querulous.fun.FunctionParam;
import com.sorma.querulous.fun.FunctionParam.ParamType;
import com.sorma.querulous.perf.PerformanceEvent;
import com.sorma.querulous.perf.PerformanceLogger;

@SuppressWarnings("unchecked")
public class DBFunctionBuilder<T> extends Connectable implements InvocationHandler {

	private PerformanceLogger performanceLogger = PerformanceLogger.getInstance();

	private static Logger logger = Logger.getLogger(DBFunctionBuilder.class);
	private final Class<T> api;
	private Map<String, DBFunctionMeta> mapping = new HashMap<String, DBFunctionMeta>();
	// private Function<ResultSet, ?> resultsetMapper = new ResultSetToMap();

	private DBFunctionBuilder(Connection connection, Class<T> api) throws SQLException {
		this.api = api;
		use(connection);
		autoBuild(connection, api);
	}

	private DBFunctionBuilder(DataSource ds, Class<T> api) throws SQLException {
		this.api = api;
		use(ds);
		Connection conn = null;
		try {
			conn = getConnection();
			autoBuild(conn, api);
		} finally {
			autoClose();
		}
	}

	private void autoBuild(Connection connection, Class<T> api) throws SQLException {
		Method[] methods = api.getMethods();

		Schema schemaAnnotation = api.getAnnotation(Schema.class);
		DB dbAnnotation = api.getAnnotation(DB.class);
		String schema = schemaAnnotation == null ? null : schemaAnnotation.value();
		String db = dbAnnotation == null ? null : dbAnnotation.value();

		DatabaseMetaData metaData = connection.getMetaData();
		for (Method method : methods) {
			String name = method.getName();
			ResultSet proc = metaData.getProcedures(db, schema, name);
			boolean found = false;
			DBFunctionMeta functionDef = null;
			while (proc.next() && !found) {
				ResultSetToMap rm = new ResultSetToMap();
				Map<String, Object> procMeta = rm.apply(proc);
				ResultSet rscol = metaData.getProcedureColumns((String)procMeta.get("PROCEDURE_CAT"), (String) procMeta.get("PROCEDURE_SCHEM"),
						(String) procMeta.get("PROCEDURE_NAME"), null);

				functionDef = procArgs(name, rscol);
				functionDef.setDb((String)procMeta.get("PROCEDURE_CAT"));
				functionDef.setSchema((String)procMeta.get("PROCEDURE_SCHEM"));

				com.sorma.querulous.annotation.TableValuedFunction funAnnot = method.getAnnotation(com.sorma.querulous.annotation.TableValuedFunction.class);
				if(funAnnot!=null){
					functionDef.setFunction(true);
					functionDef.setScalar(false);
				}
				com.sorma.querulous.annotation.ScalarValuedFunction funScalarAnnot = method.getAnnotation(com.sorma.querulous.annotation.ScalarValuedFunction.class);
				if(funScalarAnnot!=null){
					functionDef.setFunction(true);
					functionDef.setScalar(true);
				}

				mapping.put(name, functionDef);
				found = true;
			}


			if(found){
				if(functionDef.isScalar()){
					functionDef.setReturnType(ReturnType.SCALAR);
				}else{
					Class<?> returnType = method.getReturnType();
					if(returnType.equals(Void.class)){
						functionDef.setReturnType(ReturnType.VOID);
					}else if(returnType == int.class ||
							returnType == long.class ||
							Number.class.isAssignableFrom(returnType)){
						functionDef.setReturnType(ReturnType.INT);
					}else if((returnType == MultiResult.class)){
						functionDef.setReturnType(ReturnType.MULTIRESULT);
					}
				}


				Adapter adapter = method.getAnnotation(Adapter.class);
				if(functionDef.isScalar()){
					ResultSetToEntity<? extends Object> instance = new ResultSetToEntity(method.getReturnType());
					functionDef.setMapper(instance);
				}else if (adapter != null) {
					Class<? extends Function<ResultSet, ? extends Object>> rsMapper = adapter.resultsetMapper();
					if (!rsMapper.equals(Adapter.DummyMapper.class)) {
						try {
							functionDef.setMapper(rsMapper.newInstance());
						} catch (Exception e) {
							logger.error(rsMapper.getName() + " cannot be instanced as a resultset mapper.", e);
						}
					} else {
						Class<T> entityClass = (Class<T>) adapter.entityClass();
						if (!entityClass.equals(Void.class)) {
							functionDef.setMapper(new ResultSetToEntity<T>(entityClass));
						}
					}
				}
			}

		}
	}

	private DBFunctionMeta procArgs(String name, ResultSet paramsRes) throws SQLException {
		DBFunctionMeta dbFunctionMeta = new DBFunctionMeta(name);
		while (paramsRes.next()) {
			String pName = paramsRes.getString("COLUMN_NAME");
			int pType = paramsRes.getShort("COLUMN_TYPE");
			FunctionParam.ParamType colType;
			if (DatabaseMetaData.procedureColumnIn == pType) {
				colType = ParamType.IN;
			} else if (DatabaseMetaData.procedureColumnOut == pType) {
				colType = ParamType.OUT;
			} else if (DatabaseMetaData.procedureColumnInOut == pType) {
				colType = ParamType.INOUT;
			} else if (DatabaseMetaData.procedureColumnReturn == pType) {
				colType = ParamType.RESCOL;
			} else {
				colType = ParamType.UNKNOWN;
			}
			Class<?> colDataType = null;
			if (!"@TABLE_RETURN_VALUE".equals(pName)) {

				int pDataType = paramsRes.getInt("DATA_TYPE");
				switch (pDataType) {
				case Types.INTEGER:
					colDataType = Integer.class;
					break;
				case Types.TIMESTAMP:
					colDataType = Timestamp.class;
					break;
				case Types.NVARCHAR:
				case Types.VARCHAR:
				case Types.NCHAR:
				case Types.CHAR:
					colDataType = String.class;
					break;
				default:
					colDataType = Object.class;
					break;
				}
			} else {
				colType = ParamType.UNKNOWN;
			}

			dbFunctionMeta.addParam(new FunctionParam(pName.substring(1).toLowerCase(), colDataType, colType));

		}
		return dbFunctionMeta;
	}

	public static <T> T buildAdapter(Class<T> clazz, DataSource ds, Boolean commitOnClose)
			throws IllegalArgumentException, SQLException, InstantiationException {
		if (clazz.isInterface()) {
			// OK
			DBFunctionBuilder<T> builder = new DBFunctionBuilder<T>(ds, clazz);
			builder.setCommitOnClose(commitOnClose);
			T proxy = (T) Proxy.newProxyInstance(clazz.getClassLoader(), new Class[] { clazz },
					builder);
			return proxy;
		} else {
			// BOOM!
			throw new InstantiationException(clazz.getName() + " is not an Interface");
		}

	}

	public static <T> T buildAdapter(Class<T> clazz, DataSource ds) throws IllegalArgumentException, InstantiationException, SQLException{
		return buildAdapter(clazz, ds,false);
	}


	public static <T> T buildAdapter(Class<T> clazz, Connection cm)
			throws InstantiationException, IllegalAccessException, SQLException {
		return buildAdapter(clazz, cm, false);
	}

	public static <T> T buildAdapter(Class<T> clazz, Connection cm, Boolean commitOnClose)
			throws InstantiationException, IllegalAccessException, SQLException {
		if (clazz.isInterface()) {
			// OK
			DBFunctionBuilder<T> builder = new DBFunctionBuilder<T>(cm, clazz);
			builder.setCommitOnClose(commitOnClose);
			T proxy = (T) Proxy.newProxyInstance(clazz.getClassLoader(), new Class[] { clazz },
					builder);

			return proxy;
		} else {
			// BOOM!
			throw new InstantiationException(clazz.getName() + " is not an Interface");
		}
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		String name = method.getName();
		DBFunctionMeta dbFunctionMeta = mapping.get(name);
		if (dbFunctionMeta == null) {
			autoClose();
			throw new UnsupportedOperationException("Method " + method + " not mapped on DB");
		} else {
			try {
				logger.debug("Should invoke " + dbFunctionMeta);
				List<FunctionParam> parameters = dbFunctionMeta.getParameters();
				Map<String, Object> argMap = new LinkedHashMap<String, Object>();
				int i = 0;

				for (FunctionParam functionParam : parameters) {
					ParamType pType = functionParam.getParamType();
					if (pType == ParamType.IN || pType == ParamType.INOUT) {
						String name2 = functionParam.getName();
						argMap.put(name2, i < args.length ? args[i] : null);
						i++;
					}
				}
				Object result = executeQuery(argMap, dbFunctionMeta, getConnection());
				return result;
			} finally {
				autoClose();
			}

		}
	}

	protected Object executeQuery(Map<String, Object> arguments, DBFunctionMeta fn,
			Connection conn) throws SQLException {

		Object results=null;
		Map<String, FunctionParam> parameters = buildParametersMap(fn);

		String paramsString = buildParamString(parameters.size());

		ReturnType retType = fn.getReturnType();
		String statement =
				fn.isFunction() ?
					fn.isScalar()
						? String.format("select %s(%s)", fn.getQualifiedName(), paramsString)
						: String.format("select * from %s(%s)", fn.getQualifiedName(), paramsString)
				: retType == ReturnType.INT || retType == ReturnType.MULTIRESULT || retType == ReturnType.LIST
						? String.format("{? = call %s(%s)}", fn.getQualifiedName(), paramsString)
						: String.format("{call %s(%s)}", fn.getQualifiedName(), paramsString);

		logger.debug("Preparing statement: " + statement);

		try {
			PerformanceEvent evt = performanceLogger.onBegin();
			evt.setName("CALL");
			evt.setAttr("STMT", statement );
			// evt.setAttr("PARAMS", parameters.toString());
			evt.setAttr("PARAMS", arguments);


			PreparedStatement stmt = null;
			try{

				if(fn.getReturnType()==ReturnType.LIST || fn.getReturnType()==ReturnType.SCALAR){
					if(fn.isFunction()){
						stmt = conn.prepareStatement(statement);
					}else {
						stmt = conn.prepareCall(statement);
					}
					bindParameters(arguments, parameters, stmt,!fn.isFunction());

					List<Object> resultList = new ArrayList<Object>();
					results = resultList;
					ResultSet rsCS;
					boolean isRS = stmt.execute();
					if(isRS){
						rsCS = stmt.getResultSet();
					}else {
						((CallableStatement)stmt).getInt(1);
						stmt.getMoreResults();
						rsCS = stmt.getResultSet();
					}




					ResultSetToMap rsHandler = new ResultSetToMap();
					Function<ResultSet, ? extends Object> mapper = fn.getMapper()==null?rsHandler:fn.getMapper();
					while (rsCS != null && rsCS.next()) {
						resultList.add(mapper.apply(rsCS));
						if(fn.getReturnType()==ReturnType.SCALAR){
							results = resultList.isEmpty() ? null : resultList.get(0);
						}
					}
				}else if(fn.getReturnType()==ReturnType.VOID){
					stmt = conn.prepareStatement(statement);
					bindParameters(arguments, parameters, stmt,false);
					stmt.execute();
				}else if(fn.getReturnType()==ReturnType.INT){
					CallableStatement cs = conn.prepareCall(statement);
					stmt = cs;
					bindParameters(arguments, parameters, cs,true);

					cs.registerOutParameter(1, Types.INTEGER);
					cs.execute();
					Integer value = cs.getInt(1);
					results = value;

					if(cs.getMoreResults()){
						ResultSet resultSet = cs.getResultSet();
						if(resultSet!=null){
							while (resultSet.next()) {
								System.out.println(resultSet.getObject(2));
							}
						}
					}
				} else if(fn.getReturnType()==ReturnType.MULTIRESULT){
					MultiResult mr = new MultiResult();
					results = mr;
					CallableStatement cs = conn.prepareCall(statement);
					stmt = cs;
					bindParameters(arguments, parameters, cs,true);

					cs.registerOutParameter(1, Types.INTEGER);
					boolean isResultset = cs.execute();

					boolean first = true;
					while(first||cs.getMoreResults()){
						if(isResultset){
							ResultSet resultSet = cs.getResultSet();
							ResultSetToMap handler = new ResultSetToMap();
							if(resultSet!=null){
								List<Map<String, Object>> rowset = new ArrayList<Map<String,Object>>();
								mr.getResults().add(rowset);
								while (resultSet.next()) {
									Map<String, Object> row = handler.apply(resultSet);
									rowset.add(row);
								}
							}
						}else{
							Integer value = cs.getInt(1);
							mr.setValue(value);
						}
						first = false;
					}
					if(isResultset){
						Integer value = cs.getInt(1);
						mr.setValue(value);
					}
				}



			} finally {
				safeClose(stmt);
				evt.stop();
			}
		} finally {
			autoClose();
		}

		return results;
	}

	private void bindParameters(Map<String, Object> arguments, Map<String, FunctionParam> parameters,
			PreparedStatement stmt, boolean bindResultValue) throws SQLException {
		int i = 0;
		if (stmt instanceof CallableStatement && bindResultValue) {
			CallableStatement cs = (CallableStatement) stmt;
			cs.registerOutParameter(1, Types.INTEGER);
			i++;
		}

		for (String key : parameters.keySet()) {
			i++;

			Object arg = arguments.get(key);
			FunctionParam param = parameters.get(key);
			// logger.debug("Binding " + param + " to " + arg);
			logger.debug(format("ARG %-15s[%3d] = %s", param.getName(), i,arg ));
			Class<?> type = param.getJavaType();
			if (arg instanceof Date) {
				arg = new Timestamp(((Date) arg).getTime());
			}
			if (arg == null || type.isAssignableFrom(arg.getClass())) {
				if (type == String.class) {
					stmt.setString(i, (String) arg);
				} else if (type == Integer.class && arg != null) {
					stmt.setInt(i, (Integer) arg);
				} else if (type == Timestamp.class) {
					stmt.setTimestamp(i, (Timestamp) arg);
				} else {
					try {
						stmt.setObject(i, arg);
					} catch (SQLException e) {
						String message = "Error binding " + param + " to " + arg;
						logger.error(message);
						throw new RuntimeException(message,e);
					}
				}
			}
		}
	}

	private Map<String, FunctionParam> buildParametersMap(DBFunctionMeta fn) {
		Map<String, FunctionParam> parameters = new LinkedHashMap<String, FunctionParam>();
		for (FunctionParam functionParam : fn.getParameters()) {
			ParamType pType = functionParam.getParamType();
			if (pType == ParamType.IN || pType == ParamType.INOUT) {
				parameters.put(functionParam.getName().toLowerCase(), functionParam);
			}
		}
		return parameters;
	}

	private String buildParamString(int n) {
		String[] qms = new String[n];
		Arrays.fill(qms, "?");
		String paramsString = Joiner.on(",").join(qms);
		return paramsString;
	}

}
