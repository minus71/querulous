package com.sorma.querulous;

public class ReturnCode {
	private int value;

	public ReturnCode() {
		
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

}
