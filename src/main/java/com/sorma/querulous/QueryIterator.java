package com.sorma.querulous;

import java.io.Closeable;
import java.util.Iterator;

public interface QueryIterator<T> extends Iterator<T> , Closeable {
}
