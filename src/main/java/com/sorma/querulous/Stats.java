package com.sorma.querulous;

public class Stats {
	public final double count;
	public final double avg;
	public final double sum;
	public final double std;
	public Stats(double count, double avg, double sum, double std) {
		super();
		this.count = count;
		this.avg = avg;
		this.sum = sum;
		this.std = std;
	}
	@Override
	public String toString() {
		return "Stats [\n\tcount=" + count + "\n\tavg=" + avg + "\n\tsum=" + sum
				+ "\n\tstd=" + std + "]";
	}
}
