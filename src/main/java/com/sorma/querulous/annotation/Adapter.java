package com.sorma.querulous.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.sql.ResultSet;

import com.google.common.base.Function;
import com.sorma.querulous.ResultSetToMap;

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Adapter {
	/**
	 * If a target class is specified each recordset is handled
	 * by a ResultsetToEntity for the target class
	 * @return target class
	 */
	Class entityClass() default Void.class;
	/**
	 * If a resultsetMapper class is specified each recordset is handled
	 * by this mapper, the resultsetMapper takes precedence over the
	 * entityClass parameter
	 * @return target class
	 */
	Class<? extends Function<ResultSet,? extends Object>> resultsetMapper() default DummyMapper.class;

	public class DummyMapper implements Function<ResultSet,Object>{

		@Override
		public Object apply(ResultSet input) {
			return null;
		}

	}
}


