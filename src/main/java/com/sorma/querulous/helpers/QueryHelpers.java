package com.sorma.querulous.helpers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.google.common.base.Joiner;

public class QueryHelpers {

	public static String listToParams(String radix, Collection values){
		List<String> paramStrings = new ArrayList<String>();

		for (int i = 0;i< values.size(); i++) {
			String indexName = String.format(":%s%03d", radix, i);
			paramStrings.add(indexName);
		}
		return Joiner.on(",").join(paramStrings);
	}

	public  static <T> Map<String,T> listToArgumentsMap(String radix, Collection<T> values){
		LinkedHashMap<String, T> boundMap = new LinkedHashMap<String, T>();
		Iterator<T> iter = values.iterator();
		for (int i = 0;i< values.size(); i++) {
			T val = iter.next();
			String key = String.format("%s%03d", radix, i);
			boundMap.put(key, val);
		}
		return boundMap;
	}


}
