package com.sorma.querulous.helpers;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

public class DBHelpers {

	public static void safeClose(Statement stmt) {
		if (stmt != null) {
			try {
				stmt.close();
			} catch (SQLException e) {
				// Swallow
			}
		}

	}

	public static void safeClose(Connection conn) {
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				// Swallow
			}
		}

	}

	public static void safeClose(ResultSet stmt) {
		if (stmt != null) {
			try {
				stmt.close();
			} catch (SQLException e) {
				// Swallow
			}
		}
	}
	
	public static <V> V getAssignableValue(ResultSet input, int colIndex , Class<V> clazz){
		Object value;
		try {
			if (clazz.isAssignableFrom(String.class)) {
				value = input.getString(colIndex);
			}else if (clazz.isAssignableFrom(Integer.class)) {
				value = new Integer(input.getInt(colIndex));
			}else if (clazz.isAssignableFrom(BigDecimal.class)) {
				value =  input.getBigDecimal(colIndex);
			}else if (clazz.isAssignableFrom(Long.class)) {
				value =  new Long(input.getLong(colIndex));
			}else if (clazz.isAssignableFrom(Boolean.class)) {
				value =  new Boolean(input.getBoolean(colIndex));
			}else if (clazz.isAssignableFrom(Double.class)) {
				value =  new Double(input.getDouble(colIndex));
			}else if (clazz.isAssignableFrom(Float.class)) {
				value =  new Float(input.getFloat(colIndex));
			}else if (clazz.isAssignableFrom(Short.class)) {
				value =  new Short(input.getShort(colIndex));
			}else if (clazz.isAssignableFrom(byte[].class)) {
				value =  input.getBytes(colIndex);
			}else if (clazz.isAssignableFrom(Date.class)) {
				Date timestamp = input.getTimestamp(colIndex);
				value =  timestamp;
			} else {
				throw new IllegalArgumentException("Class "+clazz.getName()+" cannot accept values from a resultset.");
			}
		} catch (SQLException e) {
			throw new RuntimeException(
					"Column value is not assignable to "
							+ clazz.getName(), e);
		}
		return (V) value;

	}
	

}
