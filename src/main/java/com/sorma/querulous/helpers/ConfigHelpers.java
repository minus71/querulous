package com.sorma.querulous.helpers;

import java.lang.reflect.Method;

import org.apache.log4j.Logger;

import com.typesafe.config.Config;



public class ConfigHelpers {

	private static Logger logger = Logger.getLogger(ConfigHelpers.class);

	public static void configure(Object pojo, Config config) {
		Method[] methods = pojo.getClass().getMethods();
		for (Method method : methods) {
			String name = method.getName();
			Class<?>[] parameterTypes = method.getParameterTypes();
			if(parameterTypes.length==1 && name.startsWith("set")){
				String propName = toPropertyName(config, name);
				if(propName!=null){
					logger.debug("Found: "+propName+" in config");

					/**
					 * @TODO: Wet code .. dry out.
					 */

					Class<?> clazz = parameterTypes[0];
					if(clazz.isAssignableFrom(Integer.class) || clazz == int.class){
						try{
							int intVal = config.getInt(propName);
							method.invoke(pojo, new Integer(intVal));
							logger.debug("Configuration key "+propName+" bound to: "+intVal);
						}catch(Exception e){
							logger.warn("Configuration key "+propName+" not bound: "+e.getMessage());
						}

					}else if(clazz.isAssignableFrom(Boolean.class) || clazz == boolean.class){
						try{
							Boolean val = config.getBoolean(propName);
							method.invoke(pojo, val);
							logger.debug("Configuration key "+propName+" bound to: "+val);
						}catch(Exception e){
							logger.warn("Configuration key "+propName+" not bound: "+e.getMessage());
						}

					}else if(clazz.isAssignableFrom(String.class)){
						try{
							String val = config.getString(propName);
							method.invoke(pojo, val);
							logger.debug("Configuration key "+propName+" bound to: "+val);
						}catch(Exception e){
							logger.warn("Configuration key "+propName+" not bound: "+e.getMessage());
						}

					}else if(clazz.isAssignableFrom(Double.class)|| clazz == double.class){
						try{
							Double val = config.getDouble(propName);
							method.invoke(pojo, val);
							logger.debug("Configuration key "+propName+" bound to: "+val);
						}catch(Exception e){
							logger.warn("Configuration key "+propName+" not bound: "+e.getMessage());
						}

					}

				}
			}
		}
	}

	private static String toPropertyName(Config config, String name) {
		String candidatePropName = name.substring(3);
		String propName;
		if (config.hasPath(candidatePropName)) {
			propName = candidatePropName;
		} else {
			String javaFieldCase;
			if (candidatePropName.length() > 0) {
				javaFieldCase = candidatePropName.substring(0, 1).toLowerCase() + candidatePropName.substring(1);
			} else {
				javaFieldCase = candidatePropName;
			}
			if (config.hasPath(javaFieldCase)) {
				propName = javaFieldCase;
			} else {
				propName = null;
			}
		}
		return propName;
	}

}
