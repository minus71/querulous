package com.sorma.querulous.helpers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.base.Joiner;

public class CollectionValuedStatement {
	private String statement;


	private Map<String,Object> params = new LinkedHashMap<String, Object>();
	private static final Pattern rxArrayValue = Pattern.compile("\\[\\s*:([A-Za-z0-9_]+\\s*)\\]");

	public CollectionValuedStatement(String statement) {
		super();
		this.statement = statement;
	}



	public CollectionValuedStatement paramList(String name, Collection<?> values){
		Matcher m = rxArrayValue.matcher(statement);
		StringBuffer buf = new StringBuffer();
		while(m.find()){
			String parName = m.group(1);
			if(parName.equals(name)){
				String varNames = varNames(name, values);
				m.appendReplacement(buf, varNames );
			}
		}
		m.appendTail(buf);
		statement = buf.toString();
		return this;
	}


	private String varNames(String name, Collection<?> values) {
		List<String> names = new ArrayList<String>();
		int i = 0;
		for (Object value : values) {
			i++;
			String literalName = String.format("%s_%03d",  name, i);
			params.put(literalName, value);
			names.add(":"+literalName);
		}
		return Joiner.on(", ").join(names);
	}


	public String getStatement() {
		return statement;
	}


	public Map<String, Object> getParams() {
		return params;
	}




}
