package com.sorma.querulous;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.sorma.querulous.helpers.DBHelpers;

public abstract class Connectable {

	private Boolean autoClose = false;
	private Connection connection;
	private DataSource ds;
	private Boolean commitOnClose = false;
	
	
	

	private static Logger logger = Logger.getLogger(Connectable.class);

	public Boolean getAutoClose() {
		return autoClose;
	}



	public Connectable setAutoClose(Boolean autoClose) {
		this.autoClose = autoClose;
		return this;
	}

	public Connection getConnection() throws SQLException {
		if (connection != null) {
			return connection;
		} else {
			if (ds != null) {
				connection = ds.getConnection();
			}
			return connection;
		}
	}

	public void use(String jndiName) {
		try {
			InitialContext ctx = new InitialContext();
			try {
				ds = (javax.sql.DataSource) ctx.lookup(jndiName);
				this.autoClose = true;
			} catch (NamingException e) {
				logger.error("Error initializing JNDI DataSource :" + jndiName,
						e);
			}
		} catch (NamingException e) {
			logger.error("Error initializing jndi context", e);
		}
	}

	public void use(DataSource ds) {
		if (connection != null) {
			logger.warn("Setting data source to a query which already has a connection");
		}
		this.ds = ds;
		this.autoClose = true;
	}

	public void autoClose(){
		if(autoClose){
			if(commitOnClose){
				safeCommit();
			}
			DBHelpers.safeClose(connection);
			connection=null;
		}
	}

	private void safeCommit() {
		try {
			if(connection!=null && !connection.isClosed()){
				if(!connection.getAutoCommit()){
					connection.commit();
				}
			}
		} catch (SQLException e) {
			logger.warn("Failed commit",e);
		}
	}

	public void use(Connection connection) {
		this.connection = connection;
	}



	public Boolean getCommitOnClose() {
		return commitOnClose;
	}



	public void setCommitOnClose(Boolean commitOnClose) {
		this.commitOnClose = commitOnClose;
	}

	protected void connectAs(Connectable that) {
		this.autoClose=that.autoClose;
		this.commitOnClose=that.commitOnClose;
		this.connection = that.connection;
		this.ds = that.ds;
	}
	
}
