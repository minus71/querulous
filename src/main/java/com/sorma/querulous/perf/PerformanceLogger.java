package com.sorma.querulous.perf;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


public class PerformanceLogger {
	private static PerformanceLogger self;
	private static int MAX_EVENTS = 0x8000;

	private static Logger logger = Logger.getLogger(PerformanceLogger.class);
	static {
		self = new PerformanceLogger();
	}

	private PerformanceLogger(){
	}

	public static PerformanceLogger getInstance(){
		return self;
	}

	private volatile int eventCounter = 0;
	// private Map<String, PerformanceEvent> events = Collections.synchronizedMap(new LinkedHashMap<String, PerformanceEvent>());

	// Circular buffer
	private volatile PerformanceEvent[] events = new PerformanceEvent[MAX_EVENTS];

	public synchronized PerformanceEvent onBegin() {
		int id = eventCounter++;
		int index = id % MAX_EVENTS;

		PerformanceEvent evt = new PerformanceEvent(id,this);

		events[index]=evt;
		return evt;
	}

	public void dumpTo(File fileName){
		OutputStream fos = null;
		Writer writer = null;
		try {
			fos = new FileOutputStream(fileName);
			writer = new OutputStreamWriter(fos,"utf-8");
			Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").create();

			// TreeSet<PerformanceEvent> treeSet = new TreeSet<PerformanceEvent>(PE_COMPARATOR);


			for (int i = 0; i < events.length; i++) {
				PerformanceEvent pe = events[i];
				if(pe!=null){

				}
			}

			gson.toJson(getEvents());
			writer.flush();
		} catch (Exception e) {
			logger.error("Error dumping info to file:"+fileName,e);
		}finally{
			if(!safeClose(writer)){
				safeClose(fos);
			}
		}
	}



	public static boolean safeClose(Closeable c){
		if(c!=null){
			try{
				c.close();
				return true;
			}catch(Exception e){
				//SWALLOW
			}
		}
		return false;
	}

	public List<PerformanceEvent> getEvents(){
		List<PerformanceEvent> left = new ArrayList<PerformanceEvent>();
		List<PerformanceEvent> right = new ArrayList<PerformanceEvent>();
		List<PerformanceEvent> current = right;
		int previous = -1;
		for (int i = 0; i < events.length; i++) {
			PerformanceEvent pe = events[i];
			if(pe!=null){
				if(pe.id>previous){
					current = left;
				} else {
					previous = pe.id;
				}
				current.add(pe);
			}
		}

		left.addAll(right);
		return left;
	}
}
