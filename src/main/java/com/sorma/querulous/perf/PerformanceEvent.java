package com.sorma.querulous.perf;

import static java.lang.String.format;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

public class PerformanceEvent {
	public final int id;
	private String name = "UNNAMED";
	private Date eventStart;
	private Date eventEnd;
	private Map<String, Object> context = new LinkedHashMap<String, Object>();
	transient private PerformanceLogger performanceLogger;
	private static Logger logger = Logger.getLogger(PerformanceEvent.class);

	public PerformanceEvent(int eid, PerformanceLogger performanceLogger) {
		id = eid;
		eventStart = new Date();
		this.performanceLogger = performanceLogger;
	}

	public void stop(){
		if(eventEnd==null){
			eventEnd = new Date();
			logger.debug(format("%s : %dms\n%s",name,duration(),context.toString()));
		}
	}
	
	public Date getEventStart() {
		return eventStart;
	}
	public Date getEventEnd() {
		return eventEnd;
	}
	public Map<String, Object> getContext() {
		return context;
	}
	
	public void setAttr(String name, Object value){
		getContext().put(name, value);
	}


	
	@Override
	public String toString() {
		return "PerformanceEvent [id=" + id + ", name=" + name
				+ ", eventStart=" + eventStart + ", eventEnd=" + eventEnd + "]";
	}

	@Override
	public int hashCode() {
		return id;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PerformanceEvent other = (PerformanceEvent) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long duration() {
		return eventEnd==null?null:eventEnd.getTime()-eventStart.getTime();
	}
	
	
}
