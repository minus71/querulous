package com.sorma.querulous.perf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class Cons<T> implements Iterable<T> {
	@SuppressWarnings("unchecked")
	public static final Cons Nil = new Cons(null,null);
	public final T value;
	public final Cons<T> next;
	
	private Cons(T value, Cons<T> next) {
		super();
		this.value = value;
		this.next = next;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> Cons<T> cons(T ... values){
		Cons<T> elm = Nil;
		for (int i = values.length - 1; i >= 0; i--) {
			T t = values[i];
			elm = elm.push(t);
		}
		return elm;
	}

	public Cons<T> push(T t) {
		return new Cons<T>(t, this);
	}
	
	public T head(){
		return value;
	}
	
	public Cons<T> tail(){
		return next;
	}

	@Override
	public String toString() {
		String others = next == Nil ?"Nil":"...";
		return "Cons(" + value + ", " + others + ")";
	}

	public boolean isEmpty(){
		return this != Nil;
	}
	
	public Cons<T> drop(int n){
		Cons<T> _pointer = this;
		for (int i = 0; i < n; i++) {
			check(_pointer, i);
			_pointer = _pointer.next;
		}
		return _pointer;
	}

	private void check(Cons<T> _pointer, int i) {
		if(_pointer == Nil){
			throw new IndexOutOfBoundsException("Index" + i + " outside bounds of "+this);
		}
	}

	public Cons<T> take(int n){
		Cons<T> _pointer = this;
		T[] buffer = (T[]) new Object[n];
		for (int i = 0; i < n; i++) {
			check(_pointer, i);
			buffer[i] = _pointer.value;
			_pointer = _pointer.next;
		}
		return cons(buffer);
	}
	
	public Cons<T> reverse(){
		Cons<T> _src = this;
		Cons<T> _dst = Nil;
		while(!_src.isEmpty()){
			_dst.push(_src.value);
			_src = _src.next;
		}
		return _dst;
	}
	
	@Override
	public Iterator<T> iterator() {
		final Cons cons = this;
		
		return new Iterator<T>() {
			private Cons<T> _pointer = cons;
			@Override
			public boolean hasNext() {
				return _pointer.isEmpty();
			}
			@Override
			public T next() {
				T _value = _pointer.value;
				_pointer = _pointer.next;
				return _value;
			}
			@Override
			public void remove() {
				throw new UnsupportedOperationException(this.getClass()+" is immutable");
			}
		};
	}
	
	public List<T> asList(){
		ArrayList<T> out = new ArrayList<T>();
		Iterator<T> iterator = iterator();
		while(iterator.hasNext()){
			out.add(iterator.next());
		}
		return out;
	}
	
}
