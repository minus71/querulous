package com.sorma.querulous.perf;

public class StartEvent {
	public final PerformanceEvent event;

	public StartEvent(PerformanceEvent event) {
		super();
		this.event = event;
	}

}
