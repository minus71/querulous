package com.sorma.querulous.perf;

public class EndEvent {
	public final PerformanceEvent event;

	public EndEvent(PerformanceEvent event) {
		super();
		this.event = event;
	}

}
