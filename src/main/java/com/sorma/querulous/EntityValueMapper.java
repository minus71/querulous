package com.sorma.querulous;

import java.lang.reflect.Field;
import java.sql.ResultSet;

public interface EntityValueMapper {
	Object getValue(ResultSet input, Field field, String colName);
}
