package com.sorma.querulous;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.google.common.base.Function;
import com.sorma.querulous.annotation.Column;
import com.sorma.querulous.annotation.MapValue;


public class ResultSetToEntity<T> implements Function<ResultSet, T> {


	public final Class<T> entityClass;
	private EntityValueMapper entityValueMapper = new DefaultValueMapper();





	public ResultSetToEntity(Class<T> entityClass) {
		super();
		this.entityClass = entityClass;
	}
	
	
	

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public T apply(ResultSet input) {
		T entity;
		try {
			if (entityClass.isAssignableFrom(String.class)) {
				return (T) input.getString(1);
			}else if (entityClass.isAssignableFrom(Integer.class)) {
				return (T) new Integer(input.getInt(1));
			}else if (entityClass.isAssignableFrom(BigDecimal.class)) {
				return (T) input.getBigDecimal(1);
			}else if (entityClass.isAssignableFrom(Long.class)) {
				return (T) new Long(input.getLong(1));
			}else if (entityClass.isAssignableFrom(Boolean.class)) {
				return (T) new Boolean(input.getBoolean(1));
			}else if (entityClass.isAssignableFrom(Double.class)) {
				return (T) new Double(input.getDouble(1));
			}else if (entityClass.isAssignableFrom(Float.class)) {
				return (T) new Float(input.getFloat(1));
			}else if (entityClass.isAssignableFrom(Short.class)) {
				return (T) new Short(input.getShort(1));
			}else if (entityClass.isAssignableFrom(byte[].class)) {
				return (T) input.getBytes(1);
			}else if (entityClass.isAssignableFrom(Date.class)) {
				Date timestamp = input.getTimestamp(1);
				return (T) timestamp;
			}
		} catch (SQLException e) {
			throw new RuntimeException(
					"Query did not return a single value assignable to "
							+ entityClass.getName(), e);
		}

		try {
			entity = entityClass.newInstance();
		} catch (Exception e1) {
			throw new RuntimeException("Class "+entityClass+" has no valid constructor.");
		}
		Field[] declaredFields = entityClass.getDeclaredFields();
		for (Field field : declaredFields) {
			int modifiers = field.getModifiers();
			if( !has(modifiers, Modifier.TRANSIENT) &&
				!has(modifiers, Modifier.FINAL) &&
				!has(modifiers, Modifier.STATIC)) {
				Column colAnnot = field.getAnnotation(Column.class);
				String colName = colAnnot==null?
						field.getName():
						colAnnot.value();
				Object value = getValue(input, field, colName);
				MapValue mapperAnnotation = field.getAnnotation(MapValue.class);
				field.setAccessible(true);
				if(mapperAnnotation!=null){
					Class<? extends Function> mapperClass = mapperAnnotation.value();
					try {
						Function mapper = mapperClass.newInstance();
						Object mapped = mapper.apply(value);
						setField(entity, field, mapped);
					} catch (Exception e) {
						throw new RuntimeException("Column "+colName+" of cannot be loaded by mapping "+value+" with "+mapperClass.getName(),e);
					}
				}else{
					setField(entity, field, value);
				}

			}

		}

		return entity;
	}

	protected boolean has(int fieldModifiers, int modifierValue) {
		return (fieldModifiers & modifierValue) == modifierValue;
	}

	protected void setField(T entity, Field field, Object value) {
		try {
			field.set(entity, value);
		} catch (Exception e) {
			throw new RuntimeException("field "+field.getName()+" of class " +entityClass
					+ " cannot be set.");
		}
	}

	protected Object getValue(ResultSet input, Field field, String colName) {
		return entityValueMapper.getValue(input, field, colName);
	}
	public EntityValueMapper getEntityValueMapper() {
		return entityValueMapper;
	}

	public void setEntityValueMapper(EntityValueMapper entityValueMapper) {
		this.entityValueMapper = entityValueMapper;
	}




	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((entityClass == null) ? 0 : entityClass.hashCode());
		result = prime * result + ((entityValueMapper == null) ? 0 : entityValueMapper.hashCode());
		return result;
	}




	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResultSetToEntity other = (ResultSetToEntity) obj;
		if (entityClass == null) {
			if (other.entityClass != null)
				return false;
		} else if (!entityClass.equals(other.entityClass))
			return false;
		if (entityValueMapper == null) {
			if (other.entityValueMapper != null)
				return false;
		} else if (!entityValueMapper.equals(other.entityValueMapper))
			return false;
		return true;
	}

}
