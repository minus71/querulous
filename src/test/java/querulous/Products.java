package querulous;

import java.util.Date;
import java.util.List;

import com.sorma.querulous.annotation.Adapter;
import com.sorma.querulous.annotation.Schema;

@Schema(value = "dbo")
public interface Products {
	@Adapter(entityClass = Product.class)
	List<Product> getProductsList(String machines, Date from, Date to,
			Integer kind);
}
