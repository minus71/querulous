package querulous;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import com.sorma.querulous.helpers.ConfigHelpers;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

public class ConfigHelpersTest {
	
	
	private MyPojo myPojo = new MyPojo();

	private String configText = "login {\n"
			+ "user = mario\n"
			+ "password = secret\n"
			+ "}";
	
	public class MyPojo {
		private String user;
		private String password;
		
		public String getUser() {
			return user;
		}
		public void setUser(String user) {
			this.user = user;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
	}


	@Test
	public void testConfigure() {
		Config config = ConfigFactory.parseString(configText);
		ConfigHelpers.configure(myPojo, config.getConfig("login"));
		assertEquals("mario",myPojo.getUser());
		assertEquals("secret",myPojo.getPassword());
	}


}
