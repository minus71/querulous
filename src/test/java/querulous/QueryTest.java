package querulous;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import querulous.helpers.ConnectionHelper;
import querulous.helpers.LocalConfigHelpers;

import com.sorma.querulous.DBFunctionBuilder;
import com.sorma.querulous.Query;
import com.sorma.querulous.fun.DBFunctionFactory;
import com.sorma.querulous.session.Session;
import com.sorma.querulous.session.SessionManagerImpl;
import com.typesafe.config.Config;

public class QueryTest {


	private static final long DAY = 24L*60*60*100;
	private static ConnectionHelper connHelper;
	protected static DataSource dataSource;

	protected static SessionManagerImpl sessionManager;


	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		connHelper  = new ConnectionHelper();
		dataSource = connHelper.getDataSource();
		sessionManager= new SessionManagerImpl(dataSource);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		dataSource=null;
		connHelper.shutDown();
		sessionManager.shutDown();
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws SQLException {
		List<Map<String, Object>> listRecords = Query.query("select * from IspezioniAOI.dbo.CanaliMacchine")
			.using(dataSource)
			.listRecords();
		System.out.println(listRecords);
		assertTrue(listRecords.size()>0);
	}

	@Test
	@Ignore
	public void testFunctionBuilder() throws SQLException, Exception {
		Connection connection = dataSource.getConnection();
		try{
			Products api = DBFunctionBuilder.buildAdapter(Products.class, connection);
			Date dt1 = new Date();
			Date dt0 = new Date(dt1.getTime()-DAY*7);

			List<Product> productsList = api.getProductsList("SONY3", dt0, dt1, 1);
			Product p0 = productsList.get(0);
			System.out.println(p0);
		}finally{
			connection.close();
		}
	}


	@Test

	public void testFunction() throws SQLException, Exception {
		Connection connection = dataSource.getConnection();
		try{

			ScalarFn api = DBFunctionBuilder.buildAdapter(ScalarFn.class, connection);
			Date getDSTDate = api.GetDSTDate(123, new Date());
			System.out.println(getDSTDate);
			assertNotNull(getDSTDate);

			DBFunctionFactory factory = new DBFunctionFactory();
			ScalarFn api2 = factory.getAPI(ScalarFn.class, sessionManager.currentSession());
			getDSTDate = api2.GetDSTDate(123, new Date());
			System.out.println(getDSTDate);
			assertNotNull(getDSTDate);

		}finally {
			connection.close();
		}

	}

}
