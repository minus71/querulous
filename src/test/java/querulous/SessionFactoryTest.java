package querulous;

import static org.junit.Assert.*;

import java.io.File;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.sorma.querulous.session.Session;
import com.sorma.querulous.session.SessionManager;
import com.sorma.querulous.session.SessionManagerImpl;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import querulous.helpers.ConnectionHelper;


public class SessionFactoryTest {



	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws SQLException {
		Config conf = ConfigFactory.parseFile(new File(".local/dbdev.conf")).resolve();
		DataSource ds = new ConnectionHelper().getDataSource();
		SessionManager sm = new SessionManagerImpl(ds);
		Session session = sm.currentSession();
		int one = session.query("select 1").only(Integer.class);
		assertEquals(1, one);
	}

}
