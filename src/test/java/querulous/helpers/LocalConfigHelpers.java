package querulous.helpers;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.Logger;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

public class LocalConfigHelpers {

	private static final String QUERULOUS_TEST_CONF = "querulous_test.conf";
	private static Logger logger = Logger.getLogger(LocalConfigHelpers.class);	
	private static final LocalConfigHelpers self;

	static {
		self = new LocalConfigHelpers();
	}
	
	private Config usrConf;
	
	final String HOME;
	final File HOME_DIR;
	final File LOCAL_CONF_DIR;
	
	private LocalConfigHelpers(){
		HOME = System.getProperty("user.home");
		HOME_DIR = new File(HOME);
		LOCAL_CONF_DIR = new File(HOME_DIR,".local");
		if(!LOCAL_CONF_DIR.exists()){
			LOCAL_CONF_DIR.mkdir();
		}
	}
	File defaultTestConf;
	public static synchronized LocalConfigHelpers localConfig(){
		return self;
	}
	
	public Config getDefault() {
		Config cfg = get(QUERULOUS_TEST_CONF);
		return cfg;
	}

	private Config get(String confgiName) {
		File defaultTestConf = new File(LOCAL_CONF_DIR,confgiName);
		if(!defaultTestConf.exists()){
			try {
				defaultTestConf.createNewFile();
			} catch (IOException e) {
				logger.error("Error loading default configuration",e);
			}
		}
		Config cfg = ConfigFactory.parseFile(defaultTestConf).resolve();
		return cfg;
	}
	
}
