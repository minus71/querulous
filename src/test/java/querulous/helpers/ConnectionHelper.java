package querulous.helpers;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;

import com.sorma.querulous.helpers.ConfigHelpers;

public class ConnectionHelper {
	
	public DataSource ds;
	
	
	public ConnectionHelper() {
		PoolProperties pojo = new PoolProperties();
		LocalConfigHelpers localConfig = LocalConfigHelpers.localConfig();
		ConfigHelpers.configure(pojo, localConfig.getDefault().getConfig("db"));
		ds = new DataSource(pojo);
		
	}
	
	public void shutDown(){
		if(ds!=null){
			ds.close(true);
		}
	}
	
	
	@Override
	protected void finalize() throws Throwable {
		try{
			shutDown();
		}catch(Exception e){
		}
	}
	
	public javax.sql.DataSource getDataSource(){
		return ds;
	}

}
