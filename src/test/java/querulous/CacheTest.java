package querulous;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.sorma.querulous.CachedQuery;
import com.sorma.querulous.session.Session;
import com.sorma.querulous.session.SessionManagerImpl;

import querulous.helpers.ConnectionHelper;

public class CacheTest {

	private static final long DAY = 24L * 60 * 60 * 100;
	private static ConnectionHelper connHelper;
	protected static DataSource dataSource;

	protected static SessionManagerImpl sessionManager;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		connHelper = new ConnectionHelper();
		dataSource = connHelper.getDataSource();
		sessionManager = new SessionManagerImpl(dataSource);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		dataSource = null;
		connHelper.shutDown();
		sessionManager.shutDown();
	}

	private Session session;

	@Before
	public void setUp() throws SQLException {
		session = sessionManager.currentSession();
		if(session==null) {
			throw new RuntimeException("Session is null");
		}
	}

	@After
	public void tearDown() {
		try {
			session.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testRecords() throws SQLException {

		CachedQuery<Map<String, Object>> cachedListRecords = session
				.query("select * from SGPE14F.PARTI00F where substring(PADES, 1, 1)=:letter").cachedListRecords();
		// Warm up query
		List<Map<String, Object>> _list = cachedListRecords.param("letter", "A").list();

		long t0 = System.currentTimeMillis();
		List<Map<String, Object>> list = cachedListRecords.param("letter", "R").list();
		long t1 = System.currentTimeMillis();

		List<Map<String, Object>> list2 = cachedListRecords.param("letter", "R").list();
		long t2 = System.currentTimeMillis();
		List<Map<String, Object>> list3 = cachedListRecords.param("letter", "A").list();
		long t3 = System.currentTimeMillis();
		List<Map<String, Object>> list4 = cachedListRecords.param("letter", "S").list();
		long t4 = System.currentTimeMillis();

		System.out.printf("dt1: %dms\n", t1 - t0);
		System.out.printf("dt2: %dms\n", t2 - t1);
		System.out.printf("dt3: %dms\n", t3 - t2);
		System.out.printf("dt4: %dms\n", t4 - t3);

	}

	@Test
	public void testEntities() throws SQLException {

		CachedQuery<Parti> cachedListRecords = session
				.query("select * from SGPE14F.PARTI00F where substring(PADES, 1, 1)=:letter")
				.cachedListEntities(Parti.class);

		// Warm up query
		List<Parti> _list = cachedListRecords.param("letter", "A").list();

		long t0 = System.currentTimeMillis();
		List<Parti> list = cachedListRecords.param("letter", "R").list();
		long t1 = System.currentTimeMillis();

		List<Parti> list2 = cachedListRecords.param("letter", "R").list();
		long t2 = System.currentTimeMillis();
		List<Parti> list3 = cachedListRecords.param("letter", "A").list();
		long t3 = System.currentTimeMillis();
		List<Parti> list4 = cachedListRecords.param("letter", "S").list();
		long t4 = System.currentTimeMillis();

		System.out.println(list);
		System.out.println(list2);
		System.out.println(list3);
		System.out.println(list4);

		System.out.printf("dt1: %dms\n", t1 - t0);
		System.out.printf("dt2: %dms\n", t2 - t1);
		System.out.printf("dt3: %dms\n", t3 - t2);
		System.out.printf("dt4: %dms\n", t4 - t3);

	}

	public static class Parti {
		private String PAIDF;
		private String PADES;

		public String getPAIDF() {
			return PAIDF;
		}

		public void setPAIFD(String pAIDF) {
			PAIDF = pAIDF;
		}

		public String getPADES() {
			return PADES;
		}

		public void setPADES(String pADES) {
			PADES = pADES;
		}

		@Override
		public String toString() {
			return "Parti [PAIFD=" + PAIDF + ", PADES=" + PADES + "]";
		}

	}

}
