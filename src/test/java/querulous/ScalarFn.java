package querulous;

import java.util.Date;

import com.sorma.querulous.annotation.DB;
import com.sorma.querulous.annotation.ScalarValuedFunction;
import com.sorma.querulous.annotation.Schema;

@DB("Measure")
@Schema("dbo")
public interface ScalarFn {


	@ScalarValuedFunction
	Date GetDSTDate(Integer phaseId, Date date);
}
