package querulous;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Map;

import org.junit.Test;

import com.sorma.querulous.helpers.CollectionValuedStatement;

public class CollectionValuedStatementTest {

	@Test
	public void testCollectionValuedStatement() {
		CollectionValuedStatement cvs = new CollectionValuedStatement("select * from LT where x=:xxx and LT.AssemblyFormId in ( [:values] ) ");
		cvs.paramList("values", Arrays.asList(1,2,3,4));
		Map<String, Object> params = cvs.getParams();
		// System.out.println(params);
		// System.out.println(cvs.getStatement());
		assertEquals("{values_001=1, values_002=2, values_003=3, values_004=4}",params.toString());
		assertEquals("select * from LT where x=:xxx and LT.AssemblyFormId in ( :values_001, :values_002, :values_003, :values_004 )", cvs.getStatement().trim());
	}

}
